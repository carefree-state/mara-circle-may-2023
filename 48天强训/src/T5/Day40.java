package T5;

import java.util.Scanner;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: 马拉圈
 * Date: 2023-05-12
 * Time: 16:09
 */
public class Day40 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        long[] arr = new long[21];
        arr[0] = 0;
        arr[1] = 0;
        arr[2] = 1;
        for (int i = 3; i < 21; i++) {
            arr[i] = (i - 1) * (arr[i - 1] + arr[i - 2]);
        }
        while(scanner.hasNextInt()) {
            int n = scanner.nextInt();
            System.out.println(arr[n]);
        }
    }
    public static int LIS(int[] array, int n){
        // 保存以array[i]结尾的序列最长上升子序列元素的个数
        int[] dp = new int[n];
        dp[0] = 1;
        int result = 1;
        for(int i = 1; i < n; ++i){
            // 求以array[i]结尾的序列的最长上升子序列的个数
            // 将array[i]与array[0]~array[i-1]比较
            // array[i] > array[j]: dp[i] = Math.max(dp[i], dp[j]+1)
            dp[i] = 1;
            for(int j = 0; j < i; ++j){
                if(array[i] > array[j]){
                    dp[i] = Math.max(dp[i], dp[j]+1);
                }
            }
            // 求目前最大的最长的上升子序列的长度
            result = Math.max(result, dp[i]);
        }
        // 需要在所有状态中挑选一个最大值

        return result;
    }
    public static void main2(String args[]){
        // 循环输入接收每组用例
        Scanner sc = new Scanner(System.in);
        while(sc.hasNext()){
            int n = sc.nextInt();
            int[] array = new int[n];
            for(int i = 0; i < n; ++i){
                array[i] = sc.nextInt();
            }
            // 求array数组中最长公共子序列
            System.out.println(LIS(array, n));
        }
    }

}
