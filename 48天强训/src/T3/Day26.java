package T3;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: 马拉圈
 * Date: 2023-04-18
 * Time: 12:39
 */

import java.math.BigInteger;
import java.util.*;


class Solution {



    public int jumpFloorII (int number) {
        return (int)Math.pow(2, number - 1);
    }
}
public class Day26 {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        while(scanner.hasNextBigInteger()) {
            BigInteger bigInteger1 = scanner.nextBigInteger();
            BigInteger bigInteger2 = scanner.nextBigInteger();
            bigInteger2 = bigInteger2.multiply(BigInteger.valueOf(628));
            bigInteger1 = bigInteger1.multiply(BigInteger.valueOf(100));
            if(bigInteger1.compareTo(bigInteger2) <= 0) {
                System.out.println("Yes");
            }else {
                System.out.println("No");
            }

        }
    }




    public static void main1(String[] args) {
        Solution solution = new Solution();
        System.out.println(solution.jumpFloorII(3));
    }

}
