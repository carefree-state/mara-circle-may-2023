package T5;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.util.Stack;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: 马拉圈
 * Date: 2023-05-12
 * Time: 14:06
 */
public class Day41 {


    public static boolean judgeWin(List<String> list) {

        char[][] chars = new char[20][20];
        for (int i = 0; i < 20; i++) {
            chars[i] = list.get(i).toCharArray();
        }
        for (int i = 0; i < 20; i++) {
            for (int j = 0; j < 20; j++) {
                if(chars[i][j] == '.') {
                    continue;
                }
                int countRight = 1;
                int countDown = 1;
                int countLeftDown = 1;
                int countRightDown = 1;
                for (int k = 1; k < 5; k++) {
                    if(j < 16 && chars[i][j + k] == chars[i][j]) {
                        countRight++;
                    }
                    if(i < 16 && chars[i + k][j] == chars[i][j]) {
                        countDown++;
                    }
                    if(j < 16 && i < 16 && chars[i + k][j + k] == chars[i][j]) {
                        countRightDown++;
                    }
                    if(i < 16 && j > 3 && chars[i + k][j - k] == chars[i][j]) {
                        countLeftDown++;
                    }
                }
                if(countDown == 5 || countRight == 5 || countRightDown == 5 || countLeftDown == 5) {
                    return true;
                }
            }
        }
        return false;
    }
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        while(scanner.hasNextLine()) {
            List<String> list = new ArrayList<>();
            for (int i = 0; i < 20; i++) {
                String str = scanner.nextLine();
                list.add(str);
            }
            System.out.println(judgeWin(list) ? "Yes" : "No");


        }
    }







    public static int calculate(int v1, int v2, char ch) {
        switch (ch) {
            case '+' :
                return v1 + v2;
            case '-' :
                return v1 - v2;
            case '*':
                return v1 * v2;
            case '/':
                return v1 / v2;
        }
        return 0;
    }


    public static void main1(String[] args) {
        Scanner scanner = new Scanner(System.in);
        while(scanner.hasNextInt()) {
            int n = scanner.nextInt();
            Stack<Integer> stack = new Stack<>();//后缀表达式本身就设计好了优先级
            for (int i = 0; i < n; i++) {
                String str = scanner.next();
                char ch = str.charAt(0);
                if(ch == '+' || ch == '-' || ch == '*' || ch == '/') {
                    int v2 = stack.pop();
                    int v1 = stack.pop();
                    stack.push(calculate(v1, v2, ch));
                }else {
                    stack.push(Integer.parseInt(str));
                }
            }
            System.out.println(stack.peek());
        }
    }


}
