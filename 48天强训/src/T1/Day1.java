package T1;

import java.util.HashSet;
import java.util.PriorityQueue;
import java.util.Scanner;
import java.util.Set;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: 马拉圈
 * Date: 2023-03-19
 * Time: 20:36
 */
public class Day1 {


    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        while(scanner.hasNextLine()) {
            String str1 = scanner.nextLine();
            String str2 = scanner.nextLine();
            for (int i = 0; i < str2.length(); i++) {
                String s = str2.charAt(i) + "";
                str1 = str1.replaceAll(s, "");
            }
            System.out.println(str1);
        }
    }


    public static void main1(String[] args) {

        PriorityQueue<Integer> priorityQueue = new PriorityQueue<>();
        priorityQueue.offer(1);
        priorityQueue.offer(2);
        priorityQueue.offer(3);
        priorityQueue.offer(4);
        System.out.println(priorityQueue.poll());
    }


    public static void main2(String[] args) {
        Scanner scanner = new Scanner(System.in);
        while(scanner.hasNextInt()) {
            int number = scanner.nextInt();
            PriorityQueue<Integer> priorityQueue = new PriorityQueue<>();
            for (int i = 0; i < number * 3; i++) {
                int value = scanner.nextInt();
                if(i < 2 * number) {
                    priorityQueue.offer(value);
                }else {
                    if(value > priorityQueue.peek()) {
                        priorityQueue.poll();
                        priorityQueue.offer(value);
                    }
                }
            }
            int result = 0;
            for (int i = 0; i < number; i++) {
                result += priorityQueue.poll();
                priorityQueue.poll();
            }
            System.out.println(result);
        }
    }
}
