#include "basis.h"
void menu() {
	printf("***************\n");
	printf("1. 管理员登录 \n");
	printf("0. 退出\n");
	printf("***************\n");
}
void systemMenu(Manager* pm) {
	if (pm->id <= 0) {
		printf("你好，超级管理员，%s！\n", pm->name);
	}
	else {
		printf("你好，普通管理员，%s！\n", pm->name);
	}
	printf("******************\n");
	if (pm->limits[0] == '1') {
		printf("0. 退出系统\n");
	}
	if (pm->limits[1] == '1') {
		printf("1. 卡管理\n");
	}
	if (pm->limits[2] == '1') {
		printf("2. 计费标准管理\n");
	}
	if (pm->limits[3] == '1') {
		printf("3. 计费管理\n");
	}
	if (pm->limits[4] == '1') {
		printf("4. 费用管理\n");
	}
	if (pm->limits[5] == '1') {
		printf("5. 查询统计\n");
	}
	if (pm->limits[6] == '1' && pm->id <= 0) {
		printf("6. 权限管理\n");
	}
	printf("******************\n");
}
void menu1_1() {
	printf("******************\n");
	printf("1. 添加卡\n");
	printf("2. 查询卡\n");
	printf("3. 注销卡\n");
	printf("0. 退出\n");
	printf("******************\n");
}
void menu1_2() {
	printf("******************\n");
	printf("1. 打印全部卡\n");
	printf("2. 查询具体卡\n");
	printf("0. 退出\n");
	printf("******************\n");
}
void menu1_3(Standard* pcs) {
	printf("************************************\n");
	for (int i = 1; i <= 4; i++) {
		printf("%d. ", i);
		change(i);
		if (pcs[i - 1].state == 1) {
			printf("==》单位时间内收费%.2lf元\n", pcs[i - 1].price);
		}
		else {
			printf("==》（计费标准）暂未被开发\n");
		}
	}
	printf("0. 退出\n");
	printf("************************************\n");
}
void menu1_4() {
	printf("******************\n");
	printf("1. 注销卡\n");
	printf("0. 退出\n");
	printf("※ 特别注意：注销后无法恢复!\n※ 必须由管理员重新办卡\n※ 管理员请据情况进行余额转移\n");
	printf("******************\n");
}
void menu2_1() {
	printf("******************\n");
	printf("0. 退出\n");
	printf("1. 新增计费标准\n");
	printf("2. 查询计费标准\n");
	printf("3. 删除计费标准\n");
	printf("4. 修改计费标准\n");
	printf("******************\n");
}
void menu2_2() {
	printf("******************\n");
	printf("1. 年卡\n");
	printf("2. 月卡\n");
	printf("3. 日卡\n");
	printf("4. 时卡\n");
	printf("******************\n");
}

void menu3_1() {
	printf("****************************************\n");
	printf("※ 请提示顾客：时间较长的卡请勿过早下机\n"
		"电脑时刻开着呢，不需要再次上机\n");
	printf("0. 退出\n");
	printf("1. 上机\n");
	printf("2. 下机\n");
	printf("****************************************\n");
}
void menu4_1() {
	printf("******************\n");
	printf("0. 退出\n");
	printf("1. 充值\n");
	printf("2. 退费\n");
	printf("******************\n");
}

void menu5_1() {
	printf("******************\n");
	printf("0. 退出\n");
	printf("1. 查询消费记录\n");
	printf("2. 统计总营业额\n");
	printf("3. 统计月营业额\n");
	printf("******************\n");
}
void menu5_2() {
	printf("******************\n");
	printf("1. 最近一年\n");
	printf("2. 最近一月\n");
	printf("3. 最近一天\n");
	printf("******************\n");
}
void menu6_1() {
	printf("******************\n");
	printf("0. 退出此操作\n");
	printf("1. 添加管理员\n");
	printf("2. 删除管理员\n");
	printf("******************\n");
}
void menu6_2(Manager* pm) {
	printf("******************\n");
	printf("1. 卡管理权限\n");
	printf("2. 计费标准管理权限\n");
	printf("3. 计费管理权限\n");
	printf("4. 费用管理权限\n");
	printf("5. 查询统计权限\n");
	printf("0. 结束本次操作\n");
	printf("******************\n");
}

