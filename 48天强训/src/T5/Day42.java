package T5;

import java.util.*;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: 马拉圈
 * Date: 2023-05-03
 * Time: 23:40
 */
class GraphByMatrix {

    // 1. 顶点集合
    private int[] arrayV;

    //2. 邻接矩阵
    private int[][] matrix;//顶点在这里的下标即在字符数组的下标

    //3. 是否是有向图
    private boolean isDirect;

    /**
     * @param size     【顶点个数】
     * @param isDirect
     */
    public GraphByMatrix(int size, boolean isDirect) {
        this.arrayV = new int[size];
        matrix = new int[size][size];//此时默认都是0
        this.isDirect = isDirect;

        //将邻接矩阵默认值改为【∞】
        for (int i = 0; i < size; i++) {
            Arrays.fill(matrix[i], Integer.MAX_VALUE);
            //fill,让数组充满∞这个值
        }
    }

    public void initArrayV() {
        for (int i = 0; i < 100; i++) {
            arrayV[i] =i;
        }
    }

    //获得顶点对应下标
    public int getIndexOfV(int v) {
        return v;
    }

    public void addEdge(int v1, int v2) {
        int index1 = getIndexOfV(v1);
        int index2 = getIndexOfV(v2);
        if (index1 != -1 && index2 != -1 && index1 != index2) {
            matrix[index1][index2] = 1; //index1 --> index2
            if (!isDirect) {//无向图
                matrix[index2][index1] = 1;
            }
        }
    }
    static class Point {
        int indexV;
        int distValue;

        public Point(int indexV, int distValue) {
            this.indexV = indexV;
            this.distValue = distValue;
        }
    }
    public void pQDijkstra(int src,int[] dist) {
        int srcIndex = getIndexOfV(src);
        Arrays.fill(dist, Integer.MAX_VALUE);
        dist[srcIndex] = 0;//起始点

        //定义visited数组
        int n = arrayV.length;
        boolean[] visited = new boolean[n];

        //定义小根堆
        PriorityQueue<Point> queue = new PriorityQueue<Point>(
                (o1, o2) -> {
                    return o1.distValue - o2.distValue;
                }
        );

        queue.offer(new Point(srcIndex, 0));
        while(!queue.isEmpty()) {
            Point point = queue.poll();
            int index = point.indexV;
            //被标记过，达咩！
            if(visited[index]) {
                continue;
            }
            //标记
            visited[index] = true;
            //松弛
            for (int j = 0; j < n; j++) {
                //被必要松弛到标记的顶点的，因为没用（再之前的证明中），你要也可以
                if(!visited[j] && matrix[index][j] != Integer.MAX_VALUE
                        && dist[index] + matrix[index][j] < dist[j]) {
                    //松弛导致的更新操作，更新其路径为【0，index】延伸一条边【index，j】
                    dist[j] = dist[index] + matrix[index][j];
                    queue.offer(new Point(j, dist[j]));
                }
            }
        }
    }
}
public class Day42 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        while(scanner.hasNextLine()) {
            String[] strings = new String[10];
            for (int i = 0; i < 10; i++) {
                strings[i] = scanner.nextLine();
            }
            GraphByMatrix g = new GraphByMatrix(100,true);
            g.initArrayV();
            for (int i = 0; i < 10; i++) {
                for (int j = 0; j < 10; j++) {
                    char ch = strings[i].charAt(j);
                    if(ch == '.') {
                        int from = i * 10 + j;
                        if(i + 1 < 10 && strings[i + 1].charAt(j) == '.') {
                            g.addEdge(from, from + 10);
                        }
                        if(j + 1 < 10 && strings[i].charAt(j + 1) == '.') {
                            g.addEdge(from, from + 1);
                        }
                        if(i - 1 >= 0 && strings[i - 1].charAt(j) == '.') {
                            g.addEdge(from, from - 10);
                        }
                        if(j - 1 >= 0 && strings[i].charAt(j - 1) == '.') {
                            g.addEdge(from, from - 1);
                        }
                    }
                }
            }
            int[] dist = new int[100];
            g.pQDijkstra(1, dist);
            System.out.println(dist[98]);
        }
    }

    public static void main2(String[] args) {
        Scanner scanner = new Scanner(System.in);
        while(scanner.hasNextLine()) {
            String str = scanner.nextLine();
            StringBuilder stringBuilder = new StringBuilder();
            for (int i = 0; i < str.length(); i++) {
                char ch = str.charAt(i);
                if(Character.isDigit(ch)) {
                    stringBuilder.append(ch);
                }
            }
            System.out.println(stringBuilder);
        }
    }
}
