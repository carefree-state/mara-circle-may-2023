package T4;

import jdk.nashorn.internal.ir.annotations.Ignore;

import java.util.*;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: 马拉圈
 * Date: 2023-05-06
 * Time: 12:50
 */
public class Day35 {


    public static void main(String[] args){
        Scanner scanner=new Scanner(System.in);
        //将每个名字分离出来~
        while(scanner.hasNextLine()){
            Set<String> set=new HashSet<>();
            String name=scanner.nextLine();
            int i=0;
            int end=0;
            while(i<name.length()){
                if(name.charAt(i)=='\"'){
                    end=name.indexOf("\"",i+1);
                    set.add(name.substring(i+1,end));//截取的起始位置i+1,因为i位置是"
                    i=end+2; //end指向" i需要跳过"和"后面的,
                }else {
                    end=name.indexOf(",",i+1);
                    if(end==-1){
                        //分割最后一个名字的情况
                        set.add(name.substring(i,name.length()));
                        break;
                    }
                    set.add(name.substring(i,end));//截取位置是i,因为i位置指向英文字符
                    i=end+1;//end指向, i需要跳过,
                }
            }
            name=scanner.nextLine();
            if(set.contains(name)){
                System.out.println("Ignore");
            }else {
                System.out.println("Important!");
            }
        }
    }



    public static void main1(String[] args) {
        long[] flag1 = new long[21];
        long[] flag2 = new long[21];
        flag1[0] = 0;
        flag1[1] = 0;
        flag1[2] = 1;

        flag2[0] = 1;
        flag2[1] = 1;
        flag2[2] = 2;

        for (int i = 3; i < 21; i++) {
            flag1[i] = (i - 1) * (flag1[i - 1] + flag1[i - 2]);
            flag2[i] = i * flag2[i - 1];
        }
        Scanner scanner = new Scanner(System.in);
        while(scanner.hasNextInt()) {
            int n = scanner.nextInt();
            System.out.printf("%.2f%%\n", 100.0 * flag1[n] / flag2[n]);//java没有%lf和%ld
        }
    }

}
