package T2;

import sun.font.FontRunIterator;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: 马拉圈
 * Date: 2023-04-09
 * Time: 23:03
 */
public class Day19 {

    public static int getNumber(int n) {
        int ret = 0;
        for (int i = 1; i <= n; i++) {
           if(i % 2 == 0) {
               ret++;
           }
        }
        return ret;
    }


    public static String minCommonOrder(String str1, String str2) {
        int len1 = str1.length();
        int len2 = str2.length();
        String s1 = len1 > len2 ? str2 : str1;
        String s2 = len1 <= len2 ? str2 : str1;
        for (int i = s1.length(); i > 0; i--) {
            for (int j = 0; j + i <= s1.length(); j++) {
                String str = s1.substring(j, i + j);
                if(s2.contains(str)) {
                    return str;
                }
            }
        }
        return "";
    }

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        while(scanner.hasNextLine()) {
            String str1 = scanner.nextLine();
            String str2 = scanner.nextLine();
            System.out.println(minCommonOrder(str1, str2));
        }
    }




    public static void main2(String[] args) {
        for (int i = 1; i <= 100; i++) {
            System.out.println(getNumber(i));
        }
    }



    public static void main1(String[] args) {
        Scanner scanner = new Scanner(System.in);
        List<Integer> list = new ArrayList<>();
        while(true) {
            int n = scanner.nextInt();
            if(n == 0) {
                break;
            }else {
                list.add(n);
            }
        }
        for (int i = 0; i < list.size(); i++) {
            System.out.println(getNumber(list.get(i)));
        }
    }
}
