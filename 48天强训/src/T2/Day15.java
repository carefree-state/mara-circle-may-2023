package T2;

import java.util.Arrays;
import java.util.Scanner;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: 马拉圈
 * Date: 2023-04-05
 * Time: 0:12
 */
public class Day15 {
    public static void main1(String[] args) {
        Scanner scanner = new Scanner(System.in);
        while(scanner.hasNextInt()) {
            int value = scanner.nextInt();
            int count = 0;
            while(value != 0) {
                count++;
                value &= value - 1;//减掉1个一，字面意思！
            }
            System.out.println(count);
        }
    }

    public static void main2(String[] args) {
        int[] arr = {0, 7, 1, 6, 0, 5, 6, 7};
        int[] brr = {2, 5, 0, 6, 2, 0, 7, 2};

        System.out.println(findMinimum(8, arr, brr));
    }

    public static int findMinimum(int n, int[] left, int[] right) {
        if(n == 1) {
            return 2;
        }
        // write code here
        int count = 0;
        int sum = 0;
        for (int i = 0; i < n; i++) {
            if(left[i] != 0 && right[i] == 0) {
                count += left[i];
            }
            if(left[i] == 0) {
                sum += right[i];
                right[i] = 0;
            }
        }
        Arrays.sort(right);
        for (int i = n - 1; i >= 0; i--) {
            if(i >= 1 && right[i - 1] != 0) {
                sum += right[i];
            }else if(right[i] != 0){
                sum += 1;
            }else {
                break;
            }
        }
        return sum + count + 1;
    }


}
