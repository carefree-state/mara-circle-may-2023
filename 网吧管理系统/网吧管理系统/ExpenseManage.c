#include "basis.h"

void recharge(Card* pc) {
	printf("请输入待充值卡的卡号:>");
	int id = 0;
	scanf("%d", &id);
	while (pc != NULL) {
		if (pc->id == id) {
			printf("请输入充值金额:>");
			double money = 0.0;
			scanf("%lf", &money);
			pc->balance += money;
			printf("充值成功\n");
			printf("\n注意：如果是因为欠费导致的账号注销，注销的卡已作废\n"
				"此次充值若余额恢复非负，管理员请据情况接办理新卡并且余额保留\n");
			return;
		}
		pc = pc->next;
	}
	printf("充值失败，“可能”暂无此卡\n");
}
void refunt(Card* pc) {
	printf("请输入待退费卡的卡号:>");
	int id = 0;
	scanf("%d", &id);
	while (pc != NULL) {
		if (pc->id == id && pc->effect == 1) {//负值时必然注销
			pc->balance = 0.0;
			printf("退费成功\n");
			return;
		}
		pc = pc->next;
	}
	printf("退费失败，“可能”暂无此卡或者此卡已被注销\n");
}

void expenseManage(Manager* pm) {
	int input = 0;
	Card* pc = cardCarry();
	do {
		menu4_1();
		scanf("%d", &input);
		switch (input) {
		case 1:
			recharge(pc);
			break;
		case 2:
			refunt(pc);
			break;
		case 0:
			exitOutCard(pc);
			printf("退出成功\n");
			break;
		default:
			printf("请重新输入\n");
			break;
		}
	} while (input);
}