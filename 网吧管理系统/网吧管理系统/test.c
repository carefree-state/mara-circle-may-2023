#include "basis.h"

int managerNumber = 0;

void letUsGo(Manager* pm) {
	int input = 0;
	void(*opera[7])(Manager*) =
	{ 
		exitOut, cardManage, 
		chargeStandard, chargeManage, 
		expenseManage, searchStatistics, 
		limitsManage 
	};
	printf("--------------------------\n");
	time_t t = time(NULL);
	printf("%s\n于%s登录系统\n", pm->name, ctime(&t));
	printf("--------------------------\n");
	do {
		systemMenu(pm);
		scanf("%d", &input);
		if (pm->limits[input] != '1') {
			printf("你并没有权限");
		}
		else {
			opera[input](pm);
			//管理员链表的free，在退出登录时free
		}
	} while (input);
}
void init() {
	FILE* pfr = fopen("data\\manager.txt", "wb");
	Manager* pm1 = (Manager*)malloc(sizeof(Manager));
	Manager* pm2 = (Manager*)malloc(sizeof(Manager));
	Manager* pm3 = (Manager*)malloc(sizeof(Manager));
	Manager* pm4 = (Manager*)malloc(sizeof(Manager));
	Manager* pm5 = (Manager*)malloc(sizeof(Manager));
	Manager* pm6 = (Manager*)malloc(sizeof(Manager));
	pm1->id = 0;
	strcpy(pm1->name, "小马");
	strcpy(pm1->password, "123456");
	strcpy(pm1->limits, "1111111");
	pm2->id = -1;
	strcpy(pm2->name, "小张");
	strcpy(pm2->password, "123456");
	strcpy(pm2->limits, "1111111");
	pm3->id = -2;
	strcpy(pm3->name, "老师");
	strcpy(pm3->password, "123456");
	strcpy(pm3->limits, "1111111");
	pm4->id = 1;
	strcpy(pm4->name, "小卡拉");
	strcpy(pm4->password, "123456");
	strcpy(pm4->limits, "1110000");
	pm5->id = 2;
	strcpy(pm5->name, "小空多尼");
	strcpy(pm5->password, "123456");
	strcpy(pm5->limits, "1111110");
	pm6->id = 3;
	strcpy(pm6->name, "小林");
	strcpy(pm6->password, "123456");
	strcpy(pm6->limits, "1111110");
	managerNumber = 6;
	fwrite(&managerNumber, sizeof(int), 1, pfr);
	fwrite(pm1, sizeof(Manager), 1, pfr);
	fwrite(pm2, sizeof(Manager), 1, pfr);
	fwrite(pm3, sizeof(Manager), 1, pfr);
	fwrite(pm4, sizeof(Manager), 1, pfr);
	fwrite(pm5, sizeof(Manager), 1, pfr);
	fwrite(pm6, sizeof(Manager), 1, pfr);
	fclose(pfr);
}

void signIn() {
	printf("请输入编号，姓名以及六位密码:>");
	int id = 0;
	char name[10] = { 0 };
	char password[7] = { 0 };
	int retscan = scanf("%d%s%s", &id, name, password);
	if (retscan == 0) {//控制台输入错误~
		printf("\n由于输入错误，系统崩坏\n");
		exit(0);
	}
	//错误输入会死循环的原因在于，格式化输入错误
	FILE* pf = fopen("data\\manager.txt", "rb");
	if (pf == NULL) {
		init();
		pf = fopen("data\\manager.txt", "rb");
	}
	fread(&managerNumber, sizeof(int), 1, pf);
	if (managerNumber == 0) {
		fclose(pf);
		init();
		pf = fopen("data\\manager.txt", "rb");
	}
	Manager* pm = (Manager*)malloc(sizeof(Manager));
	fread(pm, sizeof(Manager), 1, pf);
	Manager* cur = pm;
	head = pm;
	for (int i = 1; i < managerNumber; i++) {
		Manager* newOne = (Manager*)malloc(sizeof(Manager));
		fread(newOne, sizeof(Manager), 1, pf);
		newOne->next = NULL;
		cur->next = newOne;
		cur = cur->next;
	}
	fclose(pf);
	cur = pm;
	while (cur != NULL) {
		if (id == cur->id
			&& strcmp(cur->name, name) == 0
			&& strcmp(cur->password, password) == 0) {
			
			letUsGo(cur);
			
			return;
		}
		cur = cur->next;
	}
	pf = NULL;
	printf("您暂且不是管理员或者您的卡号密码错误\n");
}

int main() {
	int input = 0;
	do {
		menu();
		scanf("%d", &input);
		switch (input) {
			case 1:
				signIn();
				break;
			case 0:
				printf("退出成功\n");
				break;
			default:
				printf("请重新输入\n");
				break;
		}
	} while (input);

	return 0;
}