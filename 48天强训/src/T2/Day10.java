package T2;

import java.util.Scanner;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: 马拉圈
 * Date: 2023-03-29
 * Time: 23:28
 */
public class Day10 {
    private static int vidence1(String str) {
        if(str.length() <= 4) {
            return 5;
        }else if(str.length() <= 7) {
            return 10;
        }else {
            return 25;
        }
    }

    private static int vidence2(int[] chArr) {
        boolean flagUp = false;
        boolean flagDown = false;
        for (int i = 65, j = 97; i < 65 + 26 && j < 97 + 26; i++, j++) {
            if(chArr[i] != 0) {
                flagDown = true;
            }
            if(chArr[j] != 0) {
                flagUp = true;
            }
        }
        if(flagUp && flagDown) {
            return 20;
        }else if(flagUp || flagDown) {
            return 10;
        }else {
            return 0;
        }
    }

    private static int vidence3(int[] chArr) {
        int count = 0;
        for (int i = 48; i < 48 + 10; i++) {
            count += chArr[i];
        }
        if(count == 0){
            return 0;
        }else if(count == 1) {
            return 10;
        }else {
            return 20;
        }
    }

    private static int vidence4(int[] chArr) {
        int count = 0;
        for (int i = 0; i < 256; i++) {
            if(!Character.isDigit(i) && !Character.isAlphabetic(i)) {
                count += chArr[i];
            }
        }
        if(count == 0) {
            return 0;
        }else if(count == 1) {
            return 10;
        }else {
            return 25;
        }
    }


    private static int vidence5(int a, int b, int c) {
        if(a == 20 && b * c > 0) {
            return 5;
        }else if(a * b * c > 0) {
            return 3;
        }else if(a * b > 0) {
            return 2;
        }else {
            return 0;
        }
    }
    private static void standard(int sum) {
        switch (sum / 10) {
            case 10:
            case 9:
                System.out.println("VERY_SECURE");
                break;
            case 8:
                System.out.println("SECURE");
                break;
            case 7:
                System.out.println("VERY_STRONG");
                break;
            case 6:
                System.out.println("STRONG");
                break;
            case 5:
                System.out.println("AVERAGE");
                break;
            default:
                if(sum >= 25) {
                    System.out.println("WEAK");
                }else {
                    System.out.println("VERY_WEAK");
                }
                break;
        }
    }
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        while(scanner.hasNextLine()) {
            String password = scanner.nextLine();
            int[] chArr = new int[256];
            for (int i = 0; i < password.length(); i++) {
                char ch = password.charAt(i);
                chArr[(int)ch]++;
            }
            int ret1 = vidence1(password);
            int ret2 = vidence2(chArr);
            int ret3 = vidence3(chArr);
            int ret4 = vidence4(chArr);
            int ret5 = vidence5(ret2, ret3, ret4);
            int sum = ret1 + ret2 + ret3 + ret4 + ret5;
            standard(sum);
        }
    }


    public boolean checkWon(int[][] board) {
        int len = board.length;
        int[] arr = new int[len * 2 + 2];
        for(int i = 0; i < len; i++) {
            for(int j = 0; j < len; j++) {
                int numb = board[i][j];
                arr[i] += numb;
                arr[j + len] += numb;
                if(i == j) {
                    arr[2 * len] += numb;
                }
                if(i + j == len - 1) {
                    arr[2 * len + 1] += numb;
                }
            }
        }
        for(int i = 0; i < 2 * len + 2; i++) {
            if(arr[i] == 3) {
                return true;
            }
        }
        return false;
    }
}
