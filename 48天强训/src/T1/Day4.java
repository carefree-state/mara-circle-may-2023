package T1;

import java.util.Scanner;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: 马拉圈
 * Date: 2023-03-23
 * Time: 10:47
 */
public class Day4 {

    public static char transfer(int number) {
        if(number <= 9) {
            return (char)(number + '0');
        }
        return (char)(number - 10 + 'A');
    }

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        while(scanner.hasNextInt()) {
            int value = scanner.nextInt();
            int tmp = value;
            if(value < 0) {
                value = -value;
            }
            int radix = scanner.nextInt();
            StringBuilder stringBuilder = new StringBuilder();
            if(value == 0) {
                System.out.println(0);
                continue;
            }
            while(value != 0) {
                int number = value % radix;
                stringBuilder.append(transfer(number));
                value /= radix;
            }
            if(tmp < 0) {
                stringBuilder.append('-');
            }
            stringBuilder = stringBuilder.reverse();
            System.out.println(stringBuilder.toString());
        }
    }
    public static void main2(String[] args) {
        Scanner scanner = new Scanner(System.in);
        while(scanner.hasNextInt()) {
            int value = scanner.nextInt();
            int radix = scanner.nextInt();
            String str = Integer.toString(value, radix);
            str = str.toUpperCase();
            System.out.println(str);
        }
    }



    public static void main1(String[] args) {
        Scanner scanner = new Scanner(System.in);
        while(scanner.hasNextInt()) {
            int[] numbs = new int[4];
            for (int i = 0; i < 4; i++) {
                numbs[i] = scanner.nextInt();
            }
            System.out.print(((numbs[0] - numbs[2]) / 2 + numbs[2]) + " ");
            int B1 = (numbs[1] - numbs[3]) / 2 + numbs[3];
            int B2 = (numbs[2] - numbs[0]) / 2;
            if(B1 != B2) {
                System.out.println("No");
                continue;
            }
            System.out.print(B1 + " ");
            System.out.print((numbs[3] - numbs[1]) / 2 + " ");
        }
    }

}
