package T3;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Scanner;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: 马拉圈
 * Date: 2023-04-21
 * Time: 17:09
 */
public class Day29 {

    public int getFirstUnFormedNum(int[] arr) {

        //只有一个元素 直接返回不可合成数为此元素+1
        if(arr.length==1)
            return arr[0]+1;

        int sum=0;//最大合成数即集合所有元素相加
        int min=Integer.MAX_VALUE;//最小合成数即集合最小元素
        for(int a:arr)
        {
            sum+=a;
            if(a<min)
                min=a;
        }
        //动态规划 类背包问题
        boolean[] dp=new boolean[sum+1];
        dp[0]=true;//使集合基本元素本身的位置可合成；
        for(int i=0;i<arr.length;i++)
        {
            for(int j=sum;j>=arr[i];j--)
                dp[j]=dp[j]||dp[j-arr[i]];//上一轮外循环的本位可合成||上一轮外循环的j-arr[i]位可合成
        }
        for(int i=min+1;i<sum;i++)//sum位和min位免检
        {
            if(!dp[i])
                return i;
        }
        return sum+1;
    }

    public static int getNumber(int n) {
        int count = 0;
        while(n >= 4) {
            n = n / 3.0 == n / 3 ? n / 3 : n / 3 + 1;
            count++;
        }
        return n == 1 ? count : count + 1;
    }


    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        List<Integer> list = new ArrayList<>();
        int input = 0;
        do {
            input = scanner.nextInt();
            list.add(input);
        }while(input != 0);
        for (int i = 0; i < list.size() - 1; i++) {
            System.out.println(getNumber(list.get(i)));
        }
    }

}
