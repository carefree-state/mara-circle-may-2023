package T1;

import javax.swing.plaf.metal.MetalIconFactory;
import java.util.Scanner;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: 马拉圈
 * Date: 2023-03-24
 * Time: 14:10
 */
public class Day5 {


    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        while(scanner.hasNextInt()) {
            int number = scanner.nextInt();
            int[] arr = new int[number];
            arr[0] = scanner.nextInt();
            long max = arr[0];
            for (int i = 1; i < number; i++) {
                arr[i] = scanner.nextInt();
                if(max < arr[i]) {
                    max = arr[i];
                }
            }
            for (int i = 2; i <= number; i++) {
                for (int j = 0; j <= arr.length - i; j++) {
                    long sum = 0;
                    for (int k = 0; k < i; k++) {
                        sum += arr[k + j];
                    }
                    if(sum > max) {
                        max = sum;
                    }
                }
            }
            System.out.println(max);
        }
    }


    public static void main1(String[] args) {
        Scanner scanner = new Scanner(System.in);
        while(scanner.hasNextLine()) {
            int count = 0;
            String A = scanner.nextLine();
            String B = scanner.nextLine();
            for (int i = 0; i <= A.length(); i++) {
                StringBuilder stringBuilder1 = new StringBuilder();
                stringBuilder1.append(A.substring(0, i));
                stringBuilder1.append(B);
                stringBuilder1.append(A.substring(i, A.length()));
                String str1 = stringBuilder1.toString();
                stringBuilder1 = stringBuilder1.reverse();
                String str2 = stringBuilder1.toString();
                if(str1.equals(str2)) {
                    count++;
                }
            }
            System.out.println(count);
        }
    }



}
