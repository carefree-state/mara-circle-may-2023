package T4;

import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: 马拉圈
 * Date: 2023-04-24
 * Time: 10:49
 */
public class Day31 {

    static int[][] arr = {{0, 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31},
            {0, 31, 29, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31}};

    public static int isLeap(int year) {
        return year % 400 == 0 || (year % 4 == 0 && year % 100 != 0) ?
                1 : 0;
    }

    public static void print1(int year, int date) {
        int day = 1;
        for (int i = 0; i < 31; i++) {
            if(date == 1) {
                day += 14;
                break;
            }
            day++;
            date = (date + 1) % 7;
        }
        System.out.println(year + "-01-" + day);
    }
    public static void print2(int year, int date) {
        int days = arr[isLeap(year)][2];
        int day = 1;
        date = (date + 31) % 7;
        for (int i = 0; i < days; i++) {
            if(date == 1) {
                day += 14;
                break;
            }
            day++;
            date = (date + 1) % 7;
        }
        System.out.println(year + "-02-" + day);
    }
    public static void print3(int year, int date) {
        date = (date + 31 + 28 + 31 + 30 + 31 + isLeap(year) - 1) % 7;//五月31号星期几
        for (int i = 31; i >= 1; i--) {
            if(date == 1) {
                System.out.println(year + "-05-" + i);
                break;
            }
            date = (date - 1 + 7) % 7;
        }
    }
    public static void print4(int year, int date) {
        date = (date + 31 + 28 + 31 + 30 + 31 + 30 + 31 + 31 + isLeap(year)) % 7;//九月1号
        for (int i = 1; i <= 30; i++) {
            if(date == 1) {
                System.out.println(year + "-09-0" + i);
                break;
            }
            date = (date + 1) % 7;
        }
    }
    public static void print5(int year, int date) {
        date = (date + 365 - 61 + isLeap(year)) % 7;//十一月1号
        for (int i = 1; i <= 31; i++) {
            if(date == 4) {
                i += 21;
                System.out.println(year + "-11-" + i);
                break;
            }
            date = (date + 1) % 7;
        }
    }
    public static void main(String[] args) {//2000年第一天是星期六
        Scanner scanner = new Scanner(System.in);

        while(scanner.hasNextInt()) {
            int year = scanner.nextInt();
            int date = 6;
            for (int i = 2000; i < year; i++) {
                int days = isLeap(i) == 1 ? 366 : 365;
                date = (date + days) % 7;
            }
            //此时date为这一年第一天是星期几
            System.out.println(year + "-01-01");
            print1(year, date);
            print2(year, date);
            print3(year, date);
            System.out.println(year + "-07-04");
            print4(year, date);
            print5(year, date);
            System.out.println(year + "-12-25");//%02d
            System.out.println();
        }

    }










    public static boolean judge(int n) {
        if(n <= 1) {
            return false;
        }
        for (int i = 2; i <= Math.sqrt(n); i++) {
            if(n % i == 0) {
                return false;
            }
        }
        return true;
    }


    public static void main1(String[] args) {
        Scanner scanner = new Scanner(System.in);
        while(scanner.hasNextInt()) {
            int n = scanner.nextInt();
            System.out.print(n + " = ");
            List<Integer> list = new ArrayList<>();
            if(judge(n)) {
                System.out.println(n);
                continue;
            }
            for (int i = 2; i <= Math.sqrt(n); i++) {
                if(n % i == 0 && judge(i)) {
                    list.add(i);
                    n /= i;
                    while(n % i == 0) {
                        list.add(i);
                        n /= i;
                    }
                    if(n == 1) {
                        break;
                    }
                    if(judge(n)) {
                        list.add(n);
                        break;
                    }
                }
            }
            int i = 0;
            for (; i < list.size() - 1; i++) {
                int p = list.get(i);
                System.out.print(p + " * ");
            }
            System.out.println(list.get(i));
        }
    }



}
