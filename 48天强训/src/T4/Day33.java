package T4;

import java.util.Scanner;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: 马拉圈
 * Date: 2023-04-26
 * Time: 14:33
 */
public class Day33 {
    public static long[] arr = new long[80];

    public static void buildLibrary() {
        arr[0] = 1;
        arr[1] = 1;
        for (int i = 2; i < 80; i++) {
            arr[i] = arr[i - 1] + arr[i - 2];
        }
    }

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        buildLibrary();
        while(scanner.hasNextInt()) {
            int from = scanner.nextInt();
            int to = scanner.nextInt();
            long sum = 0;
            for (int i = from - 1; i <= to - 1; i++) {
                sum += arr[i];
            }
            System.out.println(sum);
        }
    }






    public static void main1(String[] args) {
        Scanner scanner = new Scanner(System.in);
        while(scanner.hasNext()) {
            String bigString = scanner.next();
            String smallString = scanner.next();
            String replacedString = bigString.replaceAll(smallString, "");
            int gap = bigString.length() - replacedString.length();
            System.out.println(gap / smallString.length());
        }
    }

}
