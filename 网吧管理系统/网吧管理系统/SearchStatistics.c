#include "basis.h"


void searchConsume(Card* pc) {//三个选择，最近一天，最近一月，最近一年
	printf("请输入一张卡的卡号:>");
	int id = 0;
	scanf("%d", &id);
	Card* cur1 = pc;
	while (cardNumber != 0 && cur1 != NULL) {
		if (id == cur1->id) {
			menu5_2();
			int number = 0;
			printf("请选择一个时间段（距现在）:>");
			scanf("%d", &number);
			long long gapTime = transfer(number);
			Consume* psu = consumeCarry();
			Consume* cur2 = psu;
			printf("+------+-------------+\n");
			printf("|%-6s|%13s|\n", "卡号", "消费/元");
			printf("+------+-------------+\n");
			while (cur2 != NULL) {
				Consume* tmp = cur2;
				if (cur2->id == id && time(NULL) - cur2->timeNode <= gapTime && cur2->money != 0) {
					printf("|%-6d|%13.2lf| 结算时间===>%s", cur2->id, cur2->money, ctime(&cur2->timeNode));
					printf("+------+-------------+\n");
				}
				cur2 = cur2->next;
				free(tmp);
			}
			return;
		}
		cur1 = cur1->next;
	}
	printf("查询失败，暂无此卡\n");
}
void statisticsTime(Card* pc) {
	menu5_2();
	int number = 0;
	printf("请选择一个时间段统计总营业额（距现在）:>");
	scanf("%d", &number);
	Consume* psu = consumeCarry();
	Consume* cur = psu;
	printf("+------+-------------+\n");
	printf("|%-6s|%13s|\n", "卡号", "消费/元");
	printf("+------+-------------+\n");
	double sum = 0;
	long long gapTime = transfer(number);
	while (cur != NULL) {
		Consume* tmp = cur;
		if (time(NULL) - cur->timeNode <= gapTime && cur->money != 0) {
			printf("|%-6d|%13.2lf| 结算时间===>%s", cur->id, cur->money, ctime(&cur->timeNode));
			printf("+------+-------------+\n");
			sum += cur->money;
		}
		cur = cur->next;
		free(tmp);
	}
	printf("|%-6s|%13.2lf|\n", "共计", sum);
	printf("+------+-------------+\n");
}
int months[2][13] = { { 0, 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31 },
		{0, 31, 29, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31} };
//统计到文本文件中哦！！！
int judgeLeapYear(int year) {
	return (year % 400 == 0) || (year % 4 == 0 && year % 100 != 0);
}
Date judgeMonth(long long timeStamp) {
	long long times = 1672502400;
	int year = 2023;
	int month = 1;
	int i = 0;
	while ((times += months[judgeLeapYear(year)][month] * 86400) <= timeStamp) {
		i++;
		month = i % 12 + 1;
		if (month == 1) {
			year++;
		}
	}
	long long timestamp = times - months[judgeLeapYear(year)][month] * 86400;
	Date date = { year, month, timestamp};
	return date;
}
void statisticsMonths(Card* pc) {
	Date date = judgeMonth(time(NULL));
	int year = date.year;
	int month = date.month;
	Date dates[12];
	dates[11] = date;
	for (int i = 10; i >= 0; i--) {
		month--;
		if (month == 0) {
			year--;
			month = 12;
		}
		dates[i].timestamp = dates[i + 1].timestamp - months[judgeLeapYear(year)][month] * 86400;
		dates[i].month = month;
		dates[i].year = year;
	}
	FILE* pf = fopen("data\\statisticsMonths.txt", "w");
	Consume* psu = consumeCarry();
	Puncher* ppu = puncherCarry();
	Consume* cur = psu;
	Puncher* current = ppu;
	//做成哈希，还有这样做，构建的过程都要O(N^2)

	//这里是下机计费，所以很有可能上机次数多但是收益少
	fprintf(pf, "+-------+---------------+---------------+\n");
	fprintf(pf, "|%-7s|%15s|%15s|\n", "Month", "Hands-on times", "Total turnover");
	fprintf(pf, "+-------+---------------+---------------+\n");
	printf("+-------+--------+-------------+\n");
	printf("|%-7s|%8s|%13s|\n", "年月份", "上机次数", "月总营销额/元");
	printf("+-------+--------+-------------+\n");


	for (int i = 0; i < 12; i++) {
		cur = psu;
		current = ppu;
		Date d = dates[i];
		long long min = d.timestamp;
		long long max = min + months[judgeLeapYear(d.year)][d.month] * 86400;
		double sum = 0.0;
		int count = 0; //上机次数

		//消费与上机次数统计
		while (orderNumber != 0 && cur != NULL &&
			workNumber != 0 && current != NULL) {
			if (cur->timeNode < max && cur->timeNode >= min) {
				sum += cur->money;
			}
			if (current->timeNode < max && current->timeNode >= min) {
				count++;
			}
			cur = cur->next;
			current = current->next;
		}
		while (orderNumber != 0 && cur != NULL) {
			if (cur->timeNode < max && cur->timeNode >= min) {
				sum += cur->money;
			}
			cur = cur->next;
		}
		while (workNumber != 0 && current != NULL) {
			if (current->timeNode < max && current->timeNode >= min) {
				count++;
			}
			current = current->next;
		}


		fprintf(pf, "|%4d.%02d|%15d|%15.2lf|\n", d.year, d.month, count, sum);
		fprintf(pf, "+-------+---------------+---------------+\n");
		printf("|%4d.%02d|%8d|%13.2lf|\n", d.year, d.month, count, sum);
		printf("+-------+--------+-------------+\n");

	}
	fclose(pf);
	//free掉
	exitOutConsume(psu);
	exitOutPuncher(ppu);
}


void exitOutStatistics(Card* pc) {
	printf("退出成功\n");
}


void searchStatistics(Manager* pm) {
	int input = 0;
	Card* pc = cardCarry();
	void (*func[4])(Card * pc) = 
	{ exitOutStatistics, searchConsume, statisticsTime, statisticsMonths };
	do {
		menu5_1();
		scanf("%d", &input);
		if (input <= 3 && input >= 0) {
			func[input](pc);
		}
		else {
			printf("输入失败\n");
		}

	} while (input);
}