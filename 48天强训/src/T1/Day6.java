package T1;

import java.util.Scanner;

public class Day6 {


    public static void main1(String[] args) {
        System.out.println(StrToInt("+2147483647"));
    }

    public static int StrToInt(String str) {
        if(str.length() == 0) {
            return 0;
        }
        char ch = str.charAt(0);
        int flag = 1;
        int j = 0;
        if(ch == '-') {
            flag = -1;
            j++;
        }else if(ch == '+') {
            j++;
        }
        int sum = 0;
        for(int i = j; i < str.length(); i++) {
            ch = str.charAt(i);
            if(Character.isDigit(ch)) {
                sum = 10*sum + (ch - '0');
            }else {
                return 0;
            }
        }
        return sum * flag;
        
    }


    public static void limit(int[][] board, int i, int j) {
        if(i - 2 >= 0) {
            board[i - 2][j] = 1;
        }
        if(j - 2 >= 0) {
            board[i][j - 2] = 1;
        }
        if(i + 2 < board.length) {
            board[i + 2][j] = 1;
        }
        if(j + 2 < board[0].length) {
            board[i][j + 2] = 1;
        }
    }

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        while(scanner.hasNextInt()) {
            int H = scanner.nextInt();
            int W = scanner.nextInt();
            int[][] limitBoard = new int [H][W];
            int count = 0;
            for (int i = 0; i < H; i++) {
                for (int j = 0; j < W; j++) {
                    if(limitBoard[i][j] == 0) {
                        count++;
                        limit(limitBoard, i, j);
                    }
                }
            }
            System.out.println(count);
        }
    }




}