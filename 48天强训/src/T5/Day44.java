package T5;

import java.util.Scanner;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: 马拉圈
 * Date: 2023-05-17
 * Time: 12:52
 */
public class Day44 {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        while(scanner.hasNext()) {
            String str = scanner.next();
            StringBuilder stringBuilder = new StringBuilder();
            stringBuilder.append(str.charAt(0));
            for (int i = 1; i < str.length(); i++) {
                char ch1 = str.charAt(i - 1);
                char ch2 = str.charAt(i);
                if(ch1 == '_' && Character.isAlphabetic(ch2)) {
                    stringBuilder.append((char)(ch2 - 32));
                }else if(Character.isAlphabetic(ch2)) {
                    stringBuilder.append(ch2);
                }
            }
            System.out.println(stringBuilder);
        }
    }



    public static void main1(String[] args) {
        Scanner scanner = new Scanner(System.in);
        while(scanner.hasNextLine()) {
            String str = scanner.nextLine();
            StringBuilder stringBuilder = new StringBuilder();
            for (int i = 0; i < str.length(); i++) {
                char ch = str.charAt(i);
                if(Character.isAlphabetic(ch)) {
                    stringBuilder.append(ch);
                }else {
                    stringBuilder.append(' ');
                }
            }
            str = stringBuilder.toString();
            String[] strings = str.split(" ");
            for (int i = strings.length - 1; i > 0; i--) {
                if(!strings[i].equals("")) {
                    System.out.print(strings[i] + " ");
                }
            }
            if(!strings[0].equals("")) {
                System.out.println(strings[0]);
            }
        }
    }


}
