package T3;

import sun.font.FontRunIterator;

import java.util.Arrays;
import java.util.Scanner;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: 马拉圈
 * Date: 2023-04-15
 * Time: 1:13
 */
class Bonus {

    public static int getMost(int[][] board) {
        int max = board[0][0];
        for (int i = 0; i < 6; i++) {
            for (int j = 0; j < 6; j++) {
                if(max < board[i][j]) {
                    max = board[i][j];
                }
            }
        }
        return max;
    }



    public static int getMost1(int[][] board) {
        int sum = board[0][0];
        int i = 0;
        int j = 0;
        while(i < 5 && j < 5) {
            if(board[i + 1][j] < board[i][j + 1]) {
                sum += board[i][j + 1];
                j++;
            }else {
                sum += board[i + 1][j];
                i++;
            }
        }
        while(i < 5) {
            sum += board[i + 1][j];
            i++;
        }
        while(j < 5) {
            sum += board[i][j + 1];
            j++;
        }
        return sum;
    }
}

public class Day24 {
    public static int getWay(int[][] arr, int i, int j) {
        if(i < 0 || i >= arr.length || j < 0 || j >= arr[0].length) {
            return 0;
        }else {
            if(i == arr.length - 1 && j == arr[0].length - 1) {
                return 1;
            }
            if(i >= 1 && arr[i - 1][j] == 0) {
                System.out.printf("(%d,%d)", i - 1, j);
                getWay(arr, i - 1, j);
            }
            if(j >= 1 && arr[i][j - 1] == 0) {
                System.out.printf("(%d,%d)", i, j - 1);
                getWay(arr, i, j - 1);
            }
            if(i < arr.length - 1 && arr[i + 1][j] == 0) {
                System.out.printf("(%d,%d)", i + 1, j);
                getWay(arr, i + 1, j);
            }
            if(j < arr[0].length - 1 && arr[i][j + 1] == 0) {
                System.out.printf("(%d,%d)", i, j + 1);
                getWay(arr, i,  + 1);
            }
        }
        return 0;
    }

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        while(scanner.hasNextInt()) {
            int N = scanner.nextInt();
            int M = scanner.nextInt();
            int[][] arr = new int[N][M];
            for (int i = 0; i < N; i++) {
                for (int j = 0; j < M; j++) {
                    arr[i][j] = scanner.nextInt();
                }
            }
            System.out.println("(0,0)");
            getWay(arr, 0, 0);
        }
    }










    public static void main1(String[] args) {
        int[][] arr = new int[6][6];
        for (int i = 0; i < 6; i++) {
            for (int j = 0; j < 6; j++) {
                arr[i][j] = 100 + i + j;
                System.out.print(arr[i][j] + " ");
            }
            System.out.println();
        }
        System.out.println(Bonus.getMost(arr));
    }

}
