package T5;

import com.sun.org.apache.xalan.internal.xsltc.dom.SortingIterator;

import java.util.*;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: 马拉圈
 * Date: 2023-05-15
 * Time: 10:25
 */



public class Day43 {


    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        Map<Character, Character> map = new HashMap<>();
        for (int i = '0'; i < '9' + 1; i++) {
            map.put((char)i, (char)i);
        }
        for (int i = 'A'; i < 'Z' + 1; i++) {
            switch (i) {
                case 'A':
                case 'B':
                case 'C':
                    map.put((char)i, '2');
                    break;
                case 'D':
                case 'E':
                case 'F':
                    map.put((char)i, '3');
                    break;
                case 'G':
                case 'H':
                case 'I':
                    map.put((char)i, '4');
                    break;
                case 'J':
                case 'K':
                case 'L':
                    map.put((char)i, '5');
                    break;
                case 'M':
                case 'N':
                case 'O':
                    map.put((char)i, '6');
                    break;
                case 'P':
                case 'Q':
                case 'R':
                case 'S':
                    map.put((char)i, '7');
                    break;
                case 'T':
                case 'U':
                case 'V':
                    map.put((char)i, '8');
                    break;
                case 'W':
                case 'X':
                case 'Y':
                case 'Z':
                    map.put((char)i, '9');
                    break;
            }
        }
        while(scanner.hasNextInt()) {
            int n = scanner.nextInt();
            scanner.nextLine();
            Set<String> set = new HashSet<>();
            for (int i = 0; i < n; i++) {
                String str = scanner.nextLine().replaceAll("-", "");
                StringBuilder stringBuilder = new StringBuilder();
                for (int j = 0; j < 7; j++) {
                    if(j == 3) {
                        stringBuilder.append("-");
                    }
                    stringBuilder.append(map.get(str.charAt(j)));
                }
                set.add(stringBuilder.toString());
            }
            String[] strings = new String[set.size()];
            set.toArray(strings);
            Arrays.sort(strings);
            for (int i = 0; i < strings.length; i++) {
                System.out.println(strings[i]);
            }
            System.out.println();
        }
    }
    
    
    
    
    
    
    
    
    
    
    


    public static void main1(String[] args){
        Scanner scanner=new Scanner(System.in);
        while(scanner.hasNextInt()){
            int n=scanner.nextInt();
            int m=scanner.nextInt();
            ArrayList<Integer> list=new ArrayList<>();
            dfs1(1,0,m,n,list);
        }
    }
    public static void dfs1(int pos,int curSum,int m,int n,List<Integer> queue){
        if(curSum>m) return;
        if(curSum==m){
            for(int i=0;i<queue.size()-1;i++){
                System.out.print(queue.get(i)+" ");
            }
            System.out.println(queue.get(queue.size()-1));
            return;
        }

        for(int i=pos;i<=n;i++){
            queue.add(i);
            dfs1(i+1,curSum+i,m,n,queue);
            queue.remove(queue.size()-1);
        }
    }

    public static void main2(String[] args) {
        Scanner scanner = new Scanner(System.in);
        while(scanner.hasNextInt()) {
            int n = scanner.nextInt();
            int m = scanner.nextInt();
            List<Integer> list = new ArrayList<>();
            dfs(list, 0, n, m, 1);

        }
    }

    public static void dfs(List<Integer> list, int sum, int n, int m, int position) {

        if(sum > m) {//减少没必要的步骤
            return;
        }
        if(sum == m) {
            for (int i = 0; i < list.size() - 1; i++) {
                System.out.print(list.get(i) + " ");
            }
            System.out.println(list.get(list.size() - 1));
            return;
        }

        for (int i = position; i <= n; i++) {
            list.add(i);
            dfs(list, sum + i, n, m, i + 1);
            list.remove(list.size() - 1);
        }
    }






}
