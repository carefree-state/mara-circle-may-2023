package T3;

import java.util.Scanner;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: 马拉圈
 * Date: 2023-04-10
 * Time: 21:17
 */
public class Day20 {


    public static int judge(String str1, String str2) {
        int len1 = str1.length();
        int len2 = str2.length();
        String s1 = len1 > len2 ? str2 : str1;
        String s2 = len1 <= len2 ? str2 : str1;
        for (int i = s1.length(); i > 0; i--) {
            for (int j = 0; j <= s1.length() - i; j++) {
                String str = s1.substring(j, i + j);
                if(s2.contains(str)) {
                    return str.length();
                }
            }
        }
        return 0;
    }

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        while(scanner.hasNextLine()) {
            String str1 = scanner.nextLine();
            String str2 = scanner.nextLine();
            System.out.println(judge(str1, str2));
        }
    }


    public static void main1(String[] args) {
        Scanner scanner = new Scanner(System.in);
        while(scanner.hasNextLine()) {
            System.out.println(new StringBuilder(scanner.nextLine()).reverse().toString());
        }
    }
}
