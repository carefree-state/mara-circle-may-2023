package T5;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.PriorityQueue;
import java.util.jar.JarEntry;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: 马拉圈
 * Date: 2023-05-20
 * Time: 12:13
 */
public class Day48 {



    public static int[] clockwisePrint(int[][] mat, int n, int m) {
        int[] ret = new int[n * m];
        int[][] state = new int[n][m];
        print(ret, state, mat, n, m, 0, 0, 0);
        return ret;
    }
    public static void print(int[] ret, int[][] state, int[][] mat, int n, int m, int size, int i, int j) {
        if(size == ret.length) {
            return;
        }
        ret[size++] = mat[i][j];
        state[i][j] = 1;
        if (i - 1 < 0 && j + 1 < m || (i - 1 >= 0 && j + 1 < m && state[i - 1][j] == 1 && state[i][j + 1] == 0)) {
            print(ret, state, mat, n, m, size, i, j + 1);
        } else if (j + 1 >= m && i + 1 < n || (j + 1 < m && i + 1 < n && state[i][j + 1] == 1 && state[i + 1][j] == 0)) {
            print(ret, state, mat, n, m, size, i + 1, j);
        } else if (i + 1 >= n && j - 1 >= 0 || (i + 1 < n && j - 1 >= 0 && state[i + 1][j] == 1 && state[i][j - 1] == 0)) {
            print(ret, state, mat, n, m, size, i, j - 1);
        } else{
            print(ret, state, mat, n, m, size, i - 1, j);
        }
    }

    public int findMaxGap(int[] A, int n) {
        PriorityQueue<Integer> queue1 = new PriorityQueue<Integer>(
                (o1, o2) -> {
                    return o2.compareTo(o1);
                }
        );
        PriorityQueue<Integer> queue2 = new PriorityQueue<Integer>(
                (o1, o2) -> {
                    return o2.compareTo(o1);
                }
        );
        for (int i = 0; i < n; i++) {
            queue2.offer(A[i]);
        }
        int max = 0;
        for (int i = 0; i < n - 1; i++) {
            queue2.remove(A[i]);
            queue1.offer(A[i]);
            int abs = Math.abs(queue1.peek() - queue2.peek());
            if(abs > max) {
                max = abs;
            }
        }
        return max;
    }
    public static void main(String[] args) {
        int[][] arr = {{1, 2}, {3, 4}};
        int[] ret = clockwisePrint(arr, 2, 2);
        System.out.println(Arrays.toString(ret));
    }
}
