package T4;

import jdk.internal.org.objectweb.asm.tree.MultiANewArrayInsnNode;

import java.time.Year;
import java.util.*;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: 马拉圈
 * Date: 2023-05-08
 * Time: 15:41
 */
public class Day37 {


    public static void main2(String[] args) {
        System.out.println("123".contains("123"));
    }
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        while(scanner.hasNextInt()) {
            int n = scanner.nextInt();
            scanner.nextLine();
            String[] strings = new String[n];
            for (int i = 0; i < n; i++) {
                strings[i] = scanner.nextLine();
            }
            Arrays.sort(strings);
            for (int i = 0; i < n - 1; i++) {
                if (strings[i + 1].contains(strings[i])
                        && strings[i + 1].charAt(strings[i].length()) == '/') {
                    //1. 多组数据不存在相同的情况
                    //2. 如果不是/则说明只是恰好strings[i+1]的那个目录的前几个字母与该目录恰好相同罢了！
                    strings[i] = null;
                }
            }
            for (int i = 0; i < n; i++) {
                if(strings[i] != null) {
                    System.out.println("mkdir -p " + strings[i]);
                }
            }
            System.out.println();
        }
    }


    public static void main1(String[] args) {
        Scanner scanner = new Scanner(System.in);
        while(scanner.hasNextInt()) {
            int n = scanner.nextInt();
            scanner.nextLine();
            String[] strings = new String[n];
            for (int i = 0; i < n; i++) {
                strings[i] = scanner.nextLine();
            }
            Set<Character> set = new HashSet<>();
            int max = 0;
            for (int i = 0; i < n; i++) {
                char ch = strings[i].charAt(0);
                if(set.contains(ch)) {
                    set.remove(ch);
                }else {
                    set.add(ch);
                    if(set.size() > max) {
                        max = set.size();
                    }
                }
            }
            System.out.println(max);

        }
    }
}
