package T2;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: 马拉圈
 * Date: 2023-04-09
 * Time: 0:31
 */
public class Day18 {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        while(scanner.hasNextInt()) {
            int n = scanner.nextInt();
            List<Integer> list = new ArrayList<>();
            list.add(0);
            for (int i = 1; i <= n; i++) {
                int len = list.size();
                for (int j = 0; j < len; j++) {
                    int number = list.get(j);
                    if(number >= 2) {
                        list.add(0);//生兔子了
                    }
                    list.set(j, number + 1);
                }
            }
            System.out.println(list.size());
        }


    }

















    public static boolean judge(String str1, String str2) {
        int j = 0;
        int i = 0;
        while (i < str1.length() && j < str2.length()) {
            char ch1 = str1.charAt(i);
            char ch2 = str2.charAt(j);
            if(ch1 == ch2) {
                i++;
                j++;
            }else {
                if(ch1 == '*') {
                    i++;
                    if(i >= str1.length()) {
                        return true;
                    }else {
                        ch1 = str1.charAt(i);
                    }
                    while(j < str2.length()) {
                        if (ch1 == str2.charAt(j) || ch1 == '?') {
                            break;
                        }
                        j++;
                    }

                }else if(ch1 == '?') {
                    if((j + 1 >= str2.length() && i + 1 >= str1.length())) {
                        return true;
                    }
                    if((i + 1 < str1.length() && j + 1 < str2.length()) || (i + 1 < str1.length() && str1.charAt(i + 1) == '*')) {
                        i++;
                        j++;
                    }else {
                        return false;
                    }
                }else {
                    return false;
                }
            }
        }
        return i >= str1.length() && j >= str2.length();
    }



    public static void main1(String[] args) {
        Scanner scanner = new Scanner(System.in);
        while(scanner.hasNextLine()) {
            String str1 = scanner.nextLine();
            String str2 = scanner.nextLine();
            System.out.println(judge(str1.toUpperCase(), str2.toUpperCase()));
        }
    }

}
