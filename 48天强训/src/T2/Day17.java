package T2;

import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: 马拉圈
 * Date: 2023-04-07
 * Time: 14:23
 */
class Num {
    int n;
    int i;
    public Num(int n, int i) {
        this.n = n;
        this.i = i;
    }

    @Override
    public int hashCode() {
        return super.hashCode();
    }

    @Override
    public boolean equals(Object obj) {
        return super.equals(obj);
    }
}
public class Day17 {

    public static HashMap<Num, Integer> map = new HashMap<Num, Integer>() {
        {
            this.put(new Num(1, 1), 1);
            this.put(new Num(2, 1), 1);
            this.put(new Num(2, 2), 1);
            this.put(new Num(2, 3), 1);
        }
    };

    public static int cul(int n, int i) {
        if(map.containsKey(new Num(n, i))) {
            return map.get(new Num(n, i));
        }
        if(i <= 0 || i > 2 * n - 1) {
            return 0;
        }
        if(n == 1) {
            map.put(new Num(n, i), 1);
            return 1;
        }
        int ret = cul(n - 1, i - 2) + cul(n - 1, i - 1) + cul(n - 1, i);
        map.put(new Num(n, i), ret);
        return ret;
    }

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        boolean flag = false;
        while(scanner.hasNextInt()) {
            int rows = scanner.nextInt();
            if(rows <= 2) {
                System.out.println(-1);
                flag = true;
            }else {
                if(rows % 2 == 1) {
                    System.out.println(2);
                    map.put(new Num(rows, 2), rows - 1);
                    flag = true;
                }else {
                    for (int i = 1; i <= rows; i++) {
                        if(cul(rows, i) % 2 == 0) {
                            System.out.println(i);
                            flag = true;
                            break;
                        }
                    }
                }
                if(!flag) {
                    System.out.println(-1);
                }
            }
        }
    }

    public static void main4(String[] args) {
        System.out.println(cul(4, 3));
    }

    public static void main5(String[] args) {
        Scanner scanner = new Scanner(System.in);
        boolean flag = false;
        while(scanner.hasNextInt()) {
            int rows = scanner.nextInt();
            if(rows <= 2) {
                System.out.println(-1);
                flag = true;
            }else {
                if(rows % 2 == 1) {
                    System.out.println(2);
                    flag = true;
                }else {
                    for (int i = 1; i <= rows; i++) {
                        if(cul(rows, i) % 2 == 0) {
                            System.out.println(i);
                            flag = true;
                            break;
                        }
                    }
                }
                if(!flag) {
                    System.out.println(-1);
                }
            }
        }
    }


    public static void main3(String[] args) {
        Scanner scanner = new Scanner(System.in);
        while(scanner.hasNextInt()) {
            int rows = scanner.nextInt();
            if(rows <= 2) {
                System.out.println(-1);
            }else {
                if(rows % 2 == 1) {
                    System.out.println(2);
                }else {
                    System.out.println(3);
                }
            }
        }
    }

    public static void main2(String[] args) {
        Scanner scanner = new Scanner(System.in);
        while(scanner.hasNextLine()) {
            String str = scanner.nextLine();
            str = str.toUpperCase();
            char ch = scanner.nextLine().charAt(0);
            int count = 0;
            for (int i = 0; i < str.length(); i++) {
                ch = Character.toUpperCase(ch);
                if(str.charAt(i) == ch) {
                    count++;
                }
            }
            System.out.println(count);
        }
    }
    public static void main1(String[] args) {
        Scanner scanner = new Scanner(System.in);
        while(scanner.hasNextLine()) {
            String str = scanner.nextLine();
            String ch = scanner.nextLine();
            str = str.toUpperCase();
            ch = ch.toUpperCase();
            String[] strings = str.split(ch);
            if(str.length() == 1) {
                if(str.equals(ch)) {
                    System.out.println(1);
                }else {
                    System.out.println(0);
                }
            }else {
                System.out.println(strings.length - 1);
            }
        }
    }
}
