#pragma once

//不要在这个文件开始运行程序~~~会报错!
//并且上方列表如无出现test.c，也会报错
#define _CRT_SECURE_NO_WARNINGS 1

//系统头文件
#include<stdio.h>
#include<time.h>  // 时间函数出自这
#include<string.h>
#include<stdlib.h>

//一些自定义类

//管理员
typedef struct Manager {
	int id;
	char name[10];
	char password[7];
	char limits[8];//权限
	struct Manager* next;
}Manager;

//卡
typedef struct Card {
	int id;//卡号
	char password[7];//六位密码
	double balance;//开卡金额-->余额,卡的种类决定了这个金额
	int effect; // 1-->未注销，非1-->已注销
	int cardType;//卡的种类---计费方案

	long long upTime;//上机间点

	int state;//上机与否？
	struct Card* next;//后继
}Card;

//计费标准
typedef struct Standard {
	int type;//1 2 3 4
	int state;//决定此卡是否能办
	double price;//标准单价
}Standard;
//如果是年卡月卡周卡而言，超时的时候多余的部分按一小时的间隔去计费即可！
//一开始就去扣费就好，上机的时候时间戳大于原本规定的结束时间，则更新新的结束时间
//下机的时候，如果小于原本结束时间，没有关系
//下机的时候，超过了原本的结束时间，则需要额外收费

//消费记录
typedef struct Consume {
	int id;//卡号
	long long  timeNode;//时间节点
	double money;//下机则为实际收费，合理是相同的
	struct Consume* next;//后继
}Consume;

//上机记录
typedef struct Puncher {
	int id;//卡号
	long long  timeNode;//时间节点
	struct Puncher* next;//后继
}Puncher;

//日期年月
typedef struct {
	int year;
	int month;
	long long timestamp;//首时间戳
}Date;


//申明常量

//管理员数
extern managerNumber;
//卡数
extern cardNumber;
//消费单数
extern orderNumber;
//上机单数
extern workNumber;
//润平年月份日期表
extern months[2][13];
//管理员头节点--超级全局性质
Manager* head;



//菜单
void menu();

void systemMenu(Manager* pm);

void menu1_1();
void menu1_2();
void menu1_3(Standard* pcs);
void menu1_4();

void menu2_1();
void menu2_2();

void menu3_1();

void menu4_1();

void menu5_1();
void menu5_2();

void menu6_1();
void menu6_2(Manager* pm);




//操作
// 1
void cardManage(Manager* pm);
// 2
void chargeStandard(Manager* pm);
// 3
void chargeManage(Manager* pm);
// 4
void expenseManage(Manager* pm);
// 5
void searchStatistics(Manager* pm);
// 6
void limitsManage(Manager* pm);



//退出
void exitOut(Manager* pm);
void exitOutCard(Card* pc);
void exitOutManager();
void exitOutStandard(Standard* pcs);
void exitOutConsume(Consume* psu);
void exitOutPuncher(Puncher* ppu);

//上机记录
void clockIn(Card* cur, Puncher* ppu, size_t t);
//下机记录
void settlement(Card* cur, Consume* psu, time_t t,
	double gap, Standard* pcs);


//文件读取链表
Card* cardCarry();
Standard* standardCarry();
Consume* consumeCarry();
Puncher* puncherCarry();


//特殊数字转化时间
void change(int i);
long long transfer(int i);