package T4;

import java.util.Scanner;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: 马拉圈
 * Date: 2023-05-10
 * Time: 14:05
 */
public class Day39 {

    //求出字符串大小在s1和s2之间的所有字符串的个数
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        while(scanner.hasNext()) {
            String s1 = scanner.next();
            String s2 = scanner.next();
            int len1 = scanner.nextInt();
            int len2 = scanner.nextInt();
            StringBuilder strBuilder1 = new StringBuilder(s1);
            StringBuilder strBuilder2 = new StringBuilder(s2);
            for (int i = s1.length(); i < len2; i++) {
                strBuilder1.append('a');
            }
            for (int i = s2.length(); i < len2; i++) {
                strBuilder2.append('z' + 1);
            }
            int[] array = new int[len2];
            for(int i = 0; i < len2; i++){
                array[i] = s2.charAt(i) - s1.charAt(i);
            }
            long result = 0;
            for(int i = len1; i <= len2; ++i){
                for(int j = 0; j < i; ++j){
                    result += array[j] * Math.pow(26, i - j - 1);
                }
            }
            System.out.println((result - 1)%1000007);
        }
    }
    public static int LCS(String m, String n){
        int mLen = m.length();
        int nLen = n.length();
        // 多申请一行一列：因为m和n可能存在为空字符串的情况
        int[][] dp = new int[mLen+1][nLen+1];
        for(int i = 1; i <= mLen; ++i){
            for(int j = 1; j <= nLen; ++j){
                // 如果m的i-1与n的j-1位置字符相等的情况下，则：
                // 最长公共子串为dp[i-1][j-1]+1
                if(m.charAt(i-1) == n.charAt(j-1)){
                    dp[i][j] = dp[i-1][j-1]+1;
                }else{
                    // 如果不相等，则为其上一步的最大值
                    dp[i][j] = Math.max(dp[i-1][j], dp[i][j-1]);
                }
            }
        }
        return dp[mLen][nLen];
    }
    public static void main1(String args[]){
        // 循环处理多组测试用例
        Scanner sc = new Scanner(System.in);
        while(sc.hasNext()){
            String m = sc.next();
            String n = sc.next();
            System.out.println(LCS(m,n));
        }
    }

}
