package T1;

import java.util.*;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: 马拉圈
 * Date: 2023-03-21
 * Time: 20:58
 */
public class Day3 {


    public static void main(String[] args) {
        int[] arr = {1};
        System.out.println(MoreThanHalfNum_Solution(arr));
    }
    public static int MoreThanHalfNum_Solution (int[] numbers) {
        Map<Integer, Integer> hashMap = new HashMap<>();
        for (int i = 0; i < numbers.length; i++) {
            if(hashMap.containsKey(numbers[i])) {
                int number = hashMap.get(numbers[i]);
                if(number + 1 > numbers.length / 2) {
                    return numbers[i];
                }
                hashMap.put(numbers[i],  number + 1);
            }else {
                if(numbers.length == 1) {
                    return numbers[i];
                }
                hashMap.put(numbers[i], 1);
            }
        }
        return -1;
    }

    public static void main1(String[] args) {
        Scanner scanner = new Scanner(System.in);
        while(scanner.hasNextLine()) {
            String str = scanner.nextLine();
            List<List<Character>> lists = new ArrayList<>();
            int j = 0;
            for (int i = 0; i < str.length(); i++) {
                char ch = str.charAt(i);
                lists.add(new ArrayList<>());
                while(Character.isDigit(ch)) {
                    lists.get(j).add(ch);
                    i++;
                    if(i >= str.length()) {
                        break;
                    }
                    ch = str.charAt(i);
                }
                j++;
            }
            int index = 0;
            for (int i = 0; i < lists.size(); i++) {
                if(lists.get(index).size() < lists.get(i).size()) {
                    index = i;
                }
            }
            List<Character> list = lists.get(index);
            StringBuilder stringBuilder = new StringBuilder();
            for (int i = 0; i < list.size(); i++) {
                stringBuilder.append(list.get(i));
            }
            System.out.println(stringBuilder.toString());
        }
    }
}
