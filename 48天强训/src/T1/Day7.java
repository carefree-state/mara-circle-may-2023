package T1;

import javax.xml.ws.soap.Addressing;
import java.util.*;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: 马拉圈
 * Date: 2023-03-27
 * Time: 0:17
 */
public class Day7 {

    public static boolean chkParenthesis(String A, int n) {
        Stack<Character> stack = new Stack<>();
        for (int i = 0; i < n; i++) {
            char ch = A.charAt(i);
            if(ch == '(') {
                stack.push(ch);
            }else if(ch == ')') {
                if(!stack.isEmpty() && stack.peek() == '(') {
                    stack.pop();
                }else {
                    return false;
                }
            }else {
                return false;
            }
        }
        return stack.isEmpty();
    }

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        while(scanner.hasNextInt()) {
            int value = scanner.nextInt();
            List<Integer> list = new ArrayList<>();
            list.add(0);
            list.add(1);
            list.add(1);
            if(value == 1) {
                System.out.println(0);
                continue;
            }
            int i = 3;
            do {
                int newOne = list.get(i - 1) + list.get(i - 2);
                int prev = list.get(i - 2) + list.get(i - 3);
                if(newOne >= value) {
                    int sOut = Math.min(newOne - value, value - prev);
                    System.out.println(sOut);
                    break;
                }else {
                    list.add(newOne);
                }
                i++;
            }while(true);
        }
    }



    public static void main1(String[] args) {
        Scanner scanner = new Scanner(System.in);
        while(scanner.hasNextLine()) {
            String A = scanner.nextLine();
            int n = scanner.nextInt();
            scanner.nextLine();
            System.out.println(chkParenthesis(A, n));
        }
    }
}
