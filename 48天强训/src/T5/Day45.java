package T5;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Scanner;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: 马拉圈
 * Date: 2023-05-17
 * Time: 13:21
 */
public class Day45 {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        while(scanner.hasNextInt()) {
            int n = scanner.nextInt();
            int[][] arr = new int[n][26];
            String[] strings = new String[n];
            for (int i = 0; i < n; i++) {
                String str = scanner.next();
                strings[i] = str;
                for (int j = 0; j < str.length(); j++) {
                    arr[i][str.charAt(j) - 'a']++;
                }
            }
            String str = scanner.next();
            int k = scanner.nextInt();
            List<String> list = new ArrayList<>();
            int[] nums = new int[26];
            for (int i = 0; i < str.length(); i++) {
                nums[str.charAt(i) - 'a']++;
            }
            for (int i = 0; i < n; i++) {
                if(strings[i].equals(str)) {
                    continue;
                }
                int j = 0;
                for (; j < 26; j++) {
                    if(arr[i][j] != nums[j]) {
                        break;
                    }
                }
                if(j == 26) {
                    list.add(strings[i]);
                }
            }
            Collections.sort(list);
            System.out.println(list.size());
            if(k >= list.size()) {
                continue;
            }
            System.out.println(list.get(k - 1));

        }
    }



    public static boolean isOrNo(int[] A, int[] B) {
        for (int i = 0; i < 26; i++) {
            if(A[i] < B[i]) {
                return false;
            }
        }
        return true;
    }




    public static void main1(String[] args) {
        Scanner scanner = new Scanner(System.in);
        while(scanner.hasNext()) {
            String strA = scanner.next();
            String strB = scanner.next();

            int[] arrA = new int[26];
            int[] arrB = new int[26];
            for (int i = 0; i < strA.length(); i++) {
                arrA[strA.charAt(i) - 'A']++;
            }
            for (int i = 0; i < strB.length(); i++) {
                arrB[strB.charAt(i) - 'A']++;
            }
            System.out.println(isOrNo(arrA, arrB) ? "Yes" : "No");

        }



    }

}
