#include "basis.h"

int cardNumber = 0;
void addCard(Card* pc) {
	Standard* pcs = standardCarry();
	printf("卡的类型有如下可选(开卡金额不得少于卡的类型对应的单价！)\n");
	int input = 0;
	do {
		menu1_3(pcs);
		printf("请输入卡的类型:>");
		scanf("%d", &input);
		if (input != 0) {
			Card* newOne = (Card*)malloc(sizeof(Card));
			printf("请依次输入新卡的卡号，密码，开卡金额\n");
			scanf("%d%s%lf", &newOne->id, newOne->password, &newOne->balance);
			newOne->cardType = input;
			if (pcs[newOne->cardType - 1].state != 1 || newOne->balance < pcs[newOne->cardType].price) {
				printf("添加失败,“可能”是因为开卡金额不足或者此类卡未被开发\n");
				free(newOne);
			}
			else {
				newOne->effect = 1;
				newOne->next = NULL;
				if (cardNumber == 0) {//判断数据个数是否为0，为0则说明这个节点不是有效节点
					memcpy(pc, newOne, sizeof(Card));
				}
				else {
					Card* cur = pc;
					while (cur->next != NULL) {
						cur = cur->next;
					}
					cur->next = newOne;
				}
				cardNumber++;
				printf("添加成功\n");
			}
		}
	} while (input);
	printf("退出成功\n");	
}
void printAll(Card* cur) {
	printf("+------+------+-------------+-------+---------+---------+\n");
	printf("|%-6s|%6s|%13s|%7s|%9s|%9s|\n", "卡号", "密码", "余额/元", "效应", "卡的种类", "状态");
	printf("+------+------+-------------+-------+---------+---------+\n");
	while (cur != NULL) {
		printf("|%-6d|%6s|%13.2lf|", cur->id, cur->password, cur->balance);//8.2lf代表这小数，总共站位控制在9（不足的时候补齐，足够的时候不用补齐）
		if (cur->effect == 1) {
			printf("%7s|     ", "未注销");
		}
		else {
			printf("%7s|     ", "已注销");
		}
		change(cur->cardType);
		if (cur->state == 1) {
			printf("|%9s|\n", "上机中");
		}
		else {
			printf("|%9s|\n", "未上机");
		}
		printf("+------+------+-------------+-------+---------+---------+\n");
		cur = cur->next;
	}
	printf("查找成功\n");
}
void printOne(Card* pc) {
	printf("请输入你要查询的卡号:>");
	int id = 0;
	scanf("%d", &id);
	Card* cur = pc;
	while (cardNumber != 0 && cur != NULL) {
		if (cur->id == id) {
			printf("+------+------+-------------+-------+---------+---------+\n");
			printf("|%-6s|%6s|%13s|%7s|%9s|%9s|\n", "卡号", "密码", "余额/元", "效应", "卡的种类", "状态");
			printf("+------+------+-------------+-------+---------+---------+\n");
			printf("|%-6d|%6s|%13.2lf|", cur->id, cur->password, cur->balance);//8.2lf代表这小数，总共站位控制在9（不足的时候补齐，足够的时候不用补齐）
			if (cur->effect == 1) {
				printf("%7s|     ", "未注销");
			}
			else {
				printf("%7s|     ", "已注销");
			}
			change(cur->cardType);
			if (cur->state == 1) {
				printf("|%9s|\n", "上机中");
			}
			else {
				printf("|%9s|\n", "未上机");
			}
			printf("+------+------+-------------+-------+---------+---------+\n");
			printf("查找成功\n");
			return;
		}
		cur = cur->next;
	}
	printf("查找失败，“可能”是因为暂无此卡\n");
}
void searchCard(Card* pc) {
	int input = 0;
	do {
		menu1_2();
		scanf("%d", &input);
		switch (input) {
		case 1:
			printAll(pc);
			break;
		case 2:
			printOne(pc);
			break;
		case 0:
			printf("退出成功\n");
			break;
		default:
			printf("输入失败\n");
			break;
		}
	} while (input);
}
//强制下机操作
void off(Card* pc, Consume* psu) {
	printf("由于此次下机非主动下机，所以此次消费以按比例计算（若计费标准不存在，按原计费标准计算）\n");
	Standard* pcs = standardCarry();
	time_t t = time(NULL);
	long long longTime = t - pc->upTime;
	double gap = 1.0 * longTime / transfer(pc->cardType);
	if (gap > 1) {
		settlement(pc, psu, t, gap - 1, pcs);//按本来的规则即可，按正常的比例
	}
	pc->state = 0;
}

void logOff(Card* pc) {
	Card* cur = pc;
	int id = 0;
	char password[7];
	printf("请输入待注销卡的卡号以及对应密码:>");
	scanf("%d%s", &id, password);
	while (cur != NULL && cardNumber != 0) {
		if (id == cur->id && strcmp(password, cur->password) == 0) {
			cur->effect = 0;
			printf("注销成功\n");
			if (cur->state == 1) {
				printf("已强制下机\n");
				Consume* psu = consumeCarry();
				off(cur, psu);
				exitOutConsume(psu);
			}
			return;
		}
		cur = cur->next;
	}
	printf("注销失败，“可能”是因为暂无此卡\n");
}

void logOffCard(Card* pc) {
	int input = 0;
	do {
		menu1_4();
		scanf("%d", &input);
		if (input) {
			logOff(pc);
		}
		else {
			printf("退出成功\n");
		}
	} while (input);
}

void cardManage(Manager* pm) {
	int input = 0;
	Card* pc = cardCarry();
	void (*func[4])(Card*) = 
	{ exitOutCard, addCard, searchCard, logOffCard };
	do {
		menu1_1();
		scanf("%d", &input);
		if (input >= 0 && input <= 3) {
			func[input](pc);
		}
		else {
			printf("输入失败\n");
		}
	} while (input);

}