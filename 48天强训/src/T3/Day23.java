package T3;

import java.util.Arrays;
import java.util.Scanner;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: 马拉圈
 * Date: 2023-04-14
 * Time: 14:10
 */
public class Day23 {


    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        while(scanner.hasNextLine()) {
            String str1 = scanner.nextLine();
            String str2 = scanner.nextLine();
            int count = 0;
            int len = str1.length() > str2.length() ? str2.length() : str1.length();
            for (int i = 0; i < len; i++) {
                if(str1.charAt(i) != str2.charAt(i)) {
                    count++;
                }
            }
            int ret = str1.length() + str2.length() - 2 * len + count;
            System.out.println(ret);
        }
    }



    public static int getValue(int[] gifts, int n) {
        Arrays.sort(gifts);

        int ret = gifts[n / 2];

        int left = 0;
        int right = n - 1;

        while(gifts[left] != ret) {
            left++;
        }
        while(gifts[right] != ret) {
            right--;
        }
        return right - left + 1 > n / 2 ? ret : 0;

    }


    public static void main1(String[] args) {
        int[] arr = {1, 2, 2, 1, 2};
        System.out.println(getValue(arr, arr.length));
    }




}
