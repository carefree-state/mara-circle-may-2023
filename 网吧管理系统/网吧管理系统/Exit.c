#include "basis.h"

void exitOut(Manager* pm) {
	time_t t = time(NULL);
	printf("--------------------------\n");
	printf("%s\n于%s退出系统\n", pm->name, ctime(&t));
	printf("--------------------------\n");

	exitOutManager();
}

//时间戳，1970年到现在这个点的秒数
// time.h
//time();
//ctime();返回一个字符串

void exitOutManager() {
	printf("退出成功\n");
	FILE* pf = fopen("data\\manager.txt", "wb");

	fwrite(&managerNumber, sizeof(int), 1, pf);
	//记录管理员名单
	Manager* cur = head;
	while (cur != NULL) {
		fwrite(cur, sizeof(Manager), 1, pf);
		Manager* tmp = cur;
		cur = cur->next;
		free(tmp);
	}
	//在这里一定要关闭，不然只会到程序结束，内容才会从缓存区传入文件！！！
	fclose(pf);
	pf = NULL;
}

void exitOutStandard(Standard* pcs) {
	FILE* pf = fopen("data\\rate.txt", "wb");
	fwrite(pcs, sizeof(Standard), 4, pf);
	free(pcs);
	fclose(pf);
}

void exitOutCard(Card* pc) {
	FILE* pf = fopen("data\\card.txt", "wb");
	fwrite(&cardNumber, sizeof(int), 1, pf);
	if (cardNumber == 0) {
		return;
	}
	while (pc != NULL) {
		fwrite(pc, sizeof(Card), 1, pf);
		Card* tmp = pc;
		pc = pc->next;
		free(tmp);
	}
	fclose(pf);
}

void exitOutConsume(Consume* psu) {
	FILE* pf = fopen("data\\consume.txt", "wb");
	fwrite(&orderNumber, sizeof(int), 1, pf);
	if (orderNumber == 0) {
		return;
	}
	while (psu != NULL) {
		fwrite(psu, sizeof(Consume), 1, pf);
		Consume* tmp = psu;
		psu = psu->next;
		free(tmp);
	}
	fclose(pf);
}

void exitOutPuncher(Puncher* ppu) {
	FILE* pf = fopen("data\\puncher.txt", "wb");
	fwrite(&workNumber, sizeof(int), 1, pf);
	if (workNumber == 0) {
		return;
	}
	while (ppu != NULL) {
		fwrite(ppu, sizeof(Puncher), 1, pf);
		Puncher* tmp = ppu;
		ppu = ppu->next;
		free(tmp);
	}
	fclose(pf);
}
