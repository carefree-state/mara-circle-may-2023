package T3;

import java.math.BigInteger;
import java.util.Scanner;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: 马拉圈
 * Date: 2023-04-18
 * Time: 21:07
 */
public class Day27 {
    public int Add(int num1,int num2) {
        int ret = num1 ^ num2;
        int carry = (num1 & num2) << 1;
        while(carry != 0) {
            int tmp = ret;
            ret ^= carry;
            carry = (tmp & carry) << 1;
        }
        return ret;
    }

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        while(scanner.hasNextBigInteger()) {
            BigInteger a = scanner.nextBigInteger();
            BigInteger b = scanner.nextBigInteger();
            BigInteger c = scanner.nextBigInteger();
            BigInteger ab = a.add(b);
            BigInteger bc = c.add(b);
            BigInteger ac = a.add(c);

            String str = ab.compareTo(c) > 0 && bc.compareTo(a) > 0 && ac.compareTo(b) > 0 ?
                    "Yes" : "No";
            System.out.println(str);
        }
    }

}
