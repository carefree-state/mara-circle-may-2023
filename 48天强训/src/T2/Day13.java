package T2;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.util.Stack;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: 马拉圈
 * Date: 2023-04-03
 * Time: 14:07
 */
public class Day13 {

    public static void main1(String[] args) {
        Scanner scanner = new Scanner(System.in);
        Stack<Integer> stack = new Stack<>();
        while(scanner.hasNextInt()) {
            int N = scanner.nextInt();
            int M = scanner.nextInt();
            stack.push(N);
            int count = 0;
            while(N < M) {
                boolean flag = false;
                for (int i = 2; i <= N - 1; i++) {
                    if (N % i == 0) {
                        int value = N + i;
                        stack.push(value);
                        flag = true;
                    }
                }
                if (!flag) {
                    count--;
                }
                N = stack.pop();
                count++;
                while (N > M) {
                    count--;
                    N = stack.pop();
                }
            }
            System.out.println(count + 1);
        }
    }


    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        while(scanner.hasNextLine()) {
            String str = scanner.nextLine();
            String[] strings = str.split(" ");
            List<String> list = new ArrayList<>();
            for (int i = 0; i < strings.length; i++) {
                String s = strings[i];
                if(s.charAt(0) == '"') {
                    StringBuilder stringBuilder = new StringBuilder();
                    while(i < strings.length) {
                        s = strings[i];
                        stringBuilder.append(s);
                        if((s.equals("\"") && stringBuilder.length() != 1) || (s.charAt(s.length() - 1) == '"' && !s.equals("\""))) {
                            s = stringBuilder.toString();
                            break;
                        }else {
                            stringBuilder.append(" ");
                            i++;
                        }

                    }
                    if(i == strings.length) {
                        s = stringBuilder.toString();
                    }
                }
                list.add(s);
            }
            System.out.println(list.size());
            for (int i = 0; i < list.size(); i++) {
                String ret = list.get(i);
                if (ret.charAt(ret.length() - 1) == '"') {
                    ret = ret.substring(1, ret.length() - 1);
                }
                System.out.println(ret);
            }
        }

    }
}
