package T4;

import java.lang.management.ManagementFactory;
import java.util.*;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: 马拉圈
 * Date: 2023-04-24
 * Time: 20:33
 */
public class Day32 {

    static Map<Integer, Integer> map = new HashMap<>();


    public static void getFib() {
        int flag1 = 1;
        int flag2 = 2;
        map.put(1, 1);
        map.put(2, 2);
        for (int i = 3; i <= 100000; i++) {
            int tmp = flag2;
            flag2 = (flag1 + flag2) % 1000000;
            flag1 = tmp;
            map.put(i, flag2);
        }
    }


    public static void main1(String[] args) {
        Scanner scanner = new Scanner(System.in);
        getFib();
        while(scanner.hasNextInt()) {
            int n = scanner.nextInt();
            if(n > 30) {
                System.out.printf("%06d\n", map.get(n));
            }else {
                System.out.println(map.get(n));
            }
        }
    }




    public static boolean isLeapYear(int year) {
        return year % 400 == 0 || (year % 4 == 0 && year % 100 != 0);
    }

    public static int totalYear(int year) {
        return    2 * 31
                + 1 * 28
                + 1 * 31
                + 2 * 30
                + 1 * 31
                + 2 * 30
                + 1 * 31
                + 2 * 31
                + 2 * 30
                + 2 * 31
                + 1 * 30
                + 2 * 31
                + (isLeapYear(year) ? 1 : 0);
    }

    public static boolean isPrime(int month) {
        return month == 2 || month == 3 || month == 5 || month == 7 || month == 11;
    }


    public static int detail(int year, int month, int day) {
        int ret = 0;
        ret = isPrime(month) ? day : day * 2;

        for (int i = 1; i < month; i++) {
            switch (i) {
                case 1: case 8: case 10:
                    ret += 62;
                    break;
                case 2:
                    ret += isLeapYear(year) ? 29 : 28;
                    break;
                case 3: case 5: case 7:
                    ret += 31;
                    break;
                case 4: case 6: case 9:
                    ret += 60;
                    break;
                case 11:
                    ret += 30;
                    break;
            }
        }
        return ret;
    }



    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        while(scanner.hasNextInt()) {
            int year1 = scanner.nextInt();
            int month1 = scanner.nextInt();
            int day1 = scanner.nextInt();
            int year2 = scanner.nextInt();
            int month2 = scanner.nextInt();
            int day2 = scanner.nextInt();


            int profit1ToLast = totalYear(year1) - detail(year1, month1, day1 - 1);
            int profit2FromHead = detail(year2, month2, day2);
            int profit = profit1ToLast + profit2FromHead;
            if(year1 == year2) {
                profit -= totalYear(year1);
            }else {
                for (int i = year1 + 1; i < year2; i++) {
                    profit += totalYear(i);
                }
            }
            System.out.println(profit);
        }
    }

}
