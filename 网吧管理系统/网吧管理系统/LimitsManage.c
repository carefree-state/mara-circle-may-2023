#include "basis.h"

void add(Manager* pm) {
	printf("请输入新增管理员的编号，姓名，六位密码:>");
	Manager* cur = pm;
	while (cur->next != NULL) {
		cur = cur->next;
	}
	int id = 0;
	char name[10];
	char password[7];
	scanf("%d%s%s", &id, name, password);
	if (id <= 0) {
		printf("普通管理员的id理应大于0！\n");
		return;
	}
	else {
		cur->next = (Manager*)malloc(sizeof(Manager));
		cur = cur->next;
		cur->id = id;
		strcpy(cur->name, name);
		strcpy(cur->password, password);
		strcpy(cur->limits, "0000000");
		cur->next = NULL;
	}
	int input = 0;
	menu6_2(pm);
	printf("请输入要为其设置的权限:>");
	do {
		scanf("%d", &input);
		if (input <= 5 && input >= 0) {
			cur->limits[input] = '1';
		}
		else {
			printf("输入失败\n");
		}
	} while (input);
	printf("添加成功\n");
	managerNumber++;
}


void delete(Manager* pm) {
	printf("请输入管理员的编号，姓名:>");
	//由于此操作只能由超级管理员操作并且超级管理员在最前面，
	//所以在这里往后遍历就好，但是不能删除超级管理员
	int id = 0;
	char name[10] = { 0 };
	Manager* cur = pm;
	do {
		scanf("%d%s", &id, name);
		if (id <= 0) {
			printf("无法删除!\n");
		}
	} while (id <= 0);
	while (cur->next != NULL) {
		if(id == cur->next->id && strcmp(name, cur->next->name) == 0){
			Manager* rubbish = cur->next;
			cur->next = rubbish->next;
			free(rubbish);
			printf("删除成功\n");
			managerNumber--;
			return;
		}
		cur = cur->next;
	}
	printf("此人并不是管理员\n");
}


void limitsManage(Manager* pm) {
	int input = 0;
	do {
		menu6_1();
		scanf("%d", &input);
		switch (input) {
		case 0 : 
			printf("退出成功\n");
			break;
		case 1:
			add(pm);
			break;
		case 2 : 
			delete(pm);
			break;
		default :
			printf("输入错误\n");
			break;
		}
	} while (input);
}