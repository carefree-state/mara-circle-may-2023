package T5;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: 马拉圈
 * Date: 2023-05-19
 * Time: 16:15
 */
import java.util.*;
class node implements Comparable<node>{
    int w;
    int h;
    public node(int w,int h){
        this.w=w;
        this.h=h;
    }
    public int compareTo(node o){
        int ret=this.w-o.w;
        if(ret==0){ //体重相同时，根据身高降序排列
            return o.h-this.h;
        }
        return ret;
    }
}

public class Day47{
    public static void main(String[] args){
        Scanner scanner=new Scanner(System.in);
        int n=scanner.nextInt();
        int[] arr=new int[n];
        for(int i=0;i<n;i++){
            arr[i]=scanner.nextInt();
        }
        int k=scanner.nextInt();
        int d=scanner.nextInt();
        System.out.println(getMax(arr,n,k,d));
    }
    public static long getMax(int[] arr,int n,int k,int d){
        long[][] maxV=new long[n+1][k+1];
        long[][] minV=new long[n+1][k+1];
        long ret=0;
        for(int i=1;i<=n;i++){
            maxV[i][1]=minV[i][1]=arr[i-1];
        }
        for(int i=1;i<=n;i++){
            for(int j=1;j<=k;j++){
                for(int m=i-1;m>=Math.max(i-d,1);m--){
                    maxV[i][j]=Math.max(maxV[i][j],
                            Math.max(maxV[m][j-1]*arr[i-1],minV[m][j-1]*arr[i-1]));

                    minV[i][j]=Math.min(minV[i][j],
                            Math.min(maxV[m][j-1]*arr[i-1],minV[m][j-1]*arr[i-1]));
                }
            }
            ret=Math.max(ret,maxV[i][k]);
        }
        return ret;
    }
    public static void main2(String[] args){
        Scanner scanner=new Scanner(System.in);
        while(scanner.hasNextInt()){
            int n=scanner.nextInt();
            node[] arr=new node[n];
            for(int i=0;i<n;i++){
                scanner.nextInt();
                arr[i]=new node(scanner.nextInt(),scanner.nextInt());
            }
            System.out.println(getMaxLength(arr,n));
        }
    }

    private static int getMaxLength(node[] arr,int n){
        Arrays.sort(arr);
        int[] dp=new int[n];

        int ret=1;
        for(int i=0;i<n;i++){
            dp[i]=1; //全部都初始化为1
            for(int j=0;j<i;j++){
                if(arr[i].h>=arr[j].h){
                    dp[i]=Math.max(dp[i],dp[j]+1);
                }
            }
            ret=Math.max(dp[i],ret);
        }
        return ret;
    }
}
