import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: 马拉圈
 * Date: 2023-05-22
 * Time: 15:19
 */

@WebServlet("/hello")
public class HelloServlet extends HttpServlet {//来自于刚刚的jar包
    //重写一个doGet方法
    @Override // 请求和响应
    //根据请求计算响应
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        // super.doGet(req, resp);
        System.out.println("helloWorld");
        resp.getWriter().write("hello world");

    }
}
