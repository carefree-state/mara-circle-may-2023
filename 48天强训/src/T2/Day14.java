package T2;

import java.util.Scanner;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: 马拉圈
 * Date: 2023-04-04
 * Time: 17:33
 */
public class Day14 {


    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        while(scanner.hasNextInt()) {
            int number = scanner.nextInt();
            int[] arr = new int[1000];
            int mulAll = 1;
            int sumAll = 0;
            for (int i = 0; i < number; i++) {
                int value = scanner.nextInt();
                sumAll += value;
                mulAll *= value;
                arr[0] = value;
            }
            int count = 0;
            System.out.println(count);
        }
    }
    
    
    
    public static int judge(int year) {
        return year % 400 == 0 || (year % 4 == 0 && year % 100 != 0) ?  1 : 0;
    }
    public static void main1(String[] args) {
        Scanner scanner = new Scanner(System.in);
        while(scanner.hasNextInt()) {
            int year = scanner.nextInt();
            int month = scanner.nextInt();
            int day = scanner.nextInt();
            int[][] monthDays = {{31, 28, 31, 30, 31, 30, 31, 31, 30, 31 ,30, 31}
                    , {31, 29, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31}};
            int flag = judge(year);
            int days = 0;
            for (int i = 0; i < month - 1; i++) {
                days += monthDays[flag][i];
            }
            days += day;
            System.out.println(days);
        }
    }
}
