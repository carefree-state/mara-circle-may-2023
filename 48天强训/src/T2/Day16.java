package T2;

import java.util.Scanner;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: 马拉圈
 * Date: 2023-04-06
 * Time: 13:09
 */
public class Day16 {


    public static int transfer(String str) {
        switch (str) {
            case "A":
                return 14;
            case "2":
                return 15;
            case "joker":
                return 16;
            case "JOKER":
                return 17;
            case "J":
                return 11;
            case "Q":
                return 12;
            case "K":
                return 13;
            default:
                return Integer.parseInt(str);
        }
    }

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        while(scanner.hasNext()) {
            String str = scanner.nextLine();
            String[] strings = str.split("-");
            String[] left = strings[0].split(" ");
            String[] right = strings[1].split(" ");
            int len1 = left.length;
            int len2 = right.length;
            if(len1 == len2) {
                int[] arr1 = new int[len1];
                int[] arr2 = new int[len2];
                for (int i = 0; i < len1; i++) {
                    arr1[i] = transfer(left[i]);
                    arr2[i] = transfer(right[i]);
                }
                switch (len1) {
                    case 1:
                        if(arr1[0] < arr2[0]) {
                            System.out.println(strings[1]);
                        }else {
                            System.out.println(strings[0]);
                        }
                        break;
                    case 2:
                        if((arr1[0] != arr1[1] || arr2[0] != arr2[1]) &&
                                (arr1[0] + arr1[1] != 33 && arr2[0] + arr2[1] != 33)) {
                            System.out.println("ERROR");
                        }else {
                            if(arr1[0] < arr2[0]) {
                                System.out.println(strings[1]);
                            }else {
                                System.out.println(strings[0]);
                            }
                        }
                        break;
                    case 3:
                        if(arr1[0] != arr1[1] || arr1[1] != arr1[2] ||
                                arr2[0] != arr2[1] || arr2[1] != arr2[2]) {
                            System.out.println("ERROR");
                        }else {
                            if(arr1[0] < arr2[0]) {
                                System.out.println(strings[1]);
                            }else {
                                System.out.println(strings[0]);
                            }
                        }
                        break;
                    case 4:
                        boolean flag1 = (arr1[0] != arr1[1] || arr1[1] != arr1[2] || arr1[2] != arr1[3]);//false -- 全等
                        boolean flag2 = (arr2[0] != arr2[1] || arr2[1] != arr2[2] || arr2[2] != arr2[3]);
                        if(flag1 && flag2) {
                            System.out.println("ERROR");
                        }else {
                            if(flag1 && !flag2) {
                                System.out.println(strings[1]);
                            }else if(!flag1 && flag2) {
                                System.out.println(strings[0]);
                            }else {
                                if(arr1[0] < arr2[0]) {
                                    System.out.println(strings[1]);
                                }else {
                                    System.out.println(strings[0]);
                                }
                            }
                        }
                        break;
                    case 5:
                        boolean flag3 = (arr1[0] != arr1[1] - 1 || arr1[1] != arr1[2] - 1 || arr1[2] != arr1[3] - 1);//false -- 全等
                        boolean flag4 = (arr2[0] != arr2[1] - 1|| arr2[1] != arr2[2] - 1 || arr2[2] != arr2[3] - 1);
                        if(flag3 || flag4) {
                            System.out.println("ERROR");
                        }else {
                            if(arr1[0] < arr2[0]) {
                                System.out.println(strings[1]);
                            }else {
                                System.out.println(strings[0]);
                            }
                        }
                        break;
                    default:
                        System.out.println("ERROR");
                        break;
                }

            }else {
                if(len1 == 2 && (strings[0].equals("joker JOKER") ||
                        strings[0].equals("JOKER joker"))) {
                    System.out.println(strings[0]);
                    continue;
                }
                if(len2 == 2 && strings[1].equals("joker JOKER")) {
                    System.out.println(strings[1]);
                    continue;
                }
                if(len1 == 4 && left[0].equals(left[1]) && left[1].equals(left[2])
                        && left[2].equals(left[3])) {
                    System.out.println(strings[0]);
                    continue;
                }
                if(len2 == 4 && right[0].equals(right[1]) && right[1].equals(right[2])
                        && right[2].equals(right[3])){
                    System.out.println(strings[1]);
                    continue;
                }
                System.out.println("ERROR");
            }
        }
    }







    public static int divisor(int num) {
        if(num == 1) {
            return 0;
        }
        int ret = 1;
        for (int i = 2; i <= Math.sqrt(num); i++) {
            if(num % i == 0) {
                if(i == num / i) {
                    ret += i;
                }else {
                    ret += i + num / i;
                }
            }
        }
        return ret;
    }

    public static void main1(String[] args) {
        Scanner scanner = new Scanner(System.in);
        while(scanner.hasNext()) {
            int n = scanner.nextInt();
            int count = 0;
            for (int i = 1; i < n; i++) {
                if(i == divisor(i)) {
                    count++;
                }
            }
            System.out.println(count);
        }
    }
}
