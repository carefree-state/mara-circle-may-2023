package T4;

import java.util.Scanner;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: 马拉圈
 * Date: 2023-05-06
 * Time: 14:16
 */
public class Day36 {


    //求最大公约数
    private static long calcGCD(long a, long b){
        if(0 == b){
            return a;
        }
        return calcGCD(b, a%b);
    }

    public static String transfer(long flag1, long flag2) {
        if(flag2 == 0) {
            return "lnf";
        }
        if(flag2 < 0) {
            flag2 *= -1;
            flag1 *= -1;
        }
        if(flag1 > 0) {
            long count = 0;
            count = flag1 / flag2;
            flag1 = flag1 - count * flag2;
            if(count == 0 && flag1 != 0) {
                long gcd = calcGCD(flag1, flag2);
                flag1 /= gcd;
                flag2 /= gcd;
                return  flag1 + "/" + flag2 ;
            }else if(count != 0 && flag1 != 0) {
                long gcd = calcGCD(flag1, flag2);
                flag1 /= gcd;
                flag2 /= gcd;
                return count + " " + flag1 + "/" + flag2;
            }else if(count == 0 && flag1 == 0){
                return "0";
            }else {
                return String.valueOf(count);
            }

        }else if(flag1 < 0) {
            flag1 *= -1;
            long count = 0;
            count = flag1 / flag2;
            flag1 = flag1 - count * flag2;
            if(count == 0 && flag1 != 0) {
                long gcd = calcGCD(flag1, flag2);
                flag1 /= gcd;
                flag2 /= gcd;
                return "(-" + flag1 + "/" + flag2 + ")";
            }else if(count != 0 && flag1 != 0) {
                long gcd = calcGCD(flag1, flag2);
                flag1 /= gcd;
                flag2 /= gcd;
                return "(-" + count + " " + flag1 + "/" + flag2 + ")";
            }else if(count == 0 && flag1 == 0){
                return "0";
            }else {
                return "(-" + count + ")";
            }
        }else {
            return "0";
        }

    }

    public static String add(long flag1, long flag2, long flag3, long flag4) {
        long tmp = flag2 * flag4;
        flag1 *= flag4;
        flag3 *= flag2;
        long flag = flag1 + flag3;
        return (transfer(flag, tmp));
    }
    public static String del(long flag1, long flag2, long flag3, long flag4) {
        long tmp = flag2 * flag4;
        flag1 *= flag4;
        flag3 *= flag2;
        long flag = flag1 - flag3;
        return (transfer(flag , tmp));
    }
    public static String mul(long flag1, long flag2, long flag3, long flag4) {
        flag1 *= flag3;
        flag2 *= flag4;
        return transfer(flag1 , flag2);
    }
    public static String div(long flag1, long flag2, long flag3, long flag4) {
        flag1 *= flag4;
        flag2 *= flag3;
        return transfer(flag1,flag2);
    }


    public static void main(String[] args) {
        System.out.println("(-2) / 0 = lnf".equals("(-2) / 0 = Inf"));
    }


    public static void main1(String[] args) {
        Scanner scanner = new Scanner(System.in);
        while(scanner.hasNext()) {
            String str1 = scanner.next();
            String str2 = scanner.next();

            String[] strings = str1.split("/");
            long flag1 = Long.valueOf(strings[0]);
            long flag2 = Long.valueOf(strings[1]);
            strings = str2.split("/");
            long flag3 = Long.valueOf(strings[0]);
            long flag4 = Long.valueOf(strings[1]);
            str1 = transfer(flag1, flag2);
            str2 = transfer(flag3, flag4);
            System.out.println(str1 + " + " + str2 + " = " + add(flag1,flag2,flag3,flag4));
            System.out.println(str1 + " - " + str2 + " = " + del(flag1,flag2,flag3,flag4));
            System.out.println(str1 + " * " + str2 + " = " + mul(flag1,flag2,flag3,flag4));
            System.out.println(str1 + " / " + str2 + " = " + div(flag1,flag2,flag3,flag4));



        }
    }
}
