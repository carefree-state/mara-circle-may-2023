package T3;

import java.util.Scanner;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: 马拉圈
 * Date: 2023-04-12
 * Time: 23:09
 */
public class Day22 {


    public static int getPromotedValue(int d, int a) {
        if(a >= d) {
            return d;
        }else {
            int tmp;
            while((tmp = d % a) != 0) {
                d = a;
                a = tmp;
            }
            return a;
        }
    }

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        while(scanner.hasNextInt()) {
            int n = scanner.nextInt();
            int a = scanner.nextInt();
            for (int i = 0; i < n; i++) {
                int DefenseValue = scanner.nextInt();
                a += getPromotedValue(DefenseValue, a);
            }
            System.out.println(a);
        }
    }




    public static int findFirstOne(String str) {
        int len = str.length();
        int[] arr = new int[256];
        for (int i = 0; i < len; i++) {
            char ch = str.charAt(i);
            arr[ch]++;
        }
        for (int i = 0; i < len; i++) {
            char ch = str.charAt(i);
            if(arr[ch] == 1) {
                return ch;
            }
        }
        return -1;
    }


    public static void main1(String[] args) {
        Scanner scanner = new Scanner(System.in);
        while(scanner.hasNextLine()) {
            String str = scanner.nextLine();
            int ret = findFirstOne(str);
            if(ret == -1) {
                System.out.println(-1);
            }else {
                System.out.println((char)ret);
            }
        }
    }
}
