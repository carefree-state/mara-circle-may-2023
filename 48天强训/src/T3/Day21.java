package T3;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.util.Stack;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: 马拉圈
 * Date: 2023-04-11
 * Time: 23:10
 */
public class Day21 {


    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        while(scanner.hasNextInt()) {
            int n = scanner.nextInt();
            String order = scanner.next();
            char[] orders = order.toCharArray();
            int cursor = 1;
            if(n <= 4) {
                for (int i = 0; i < orders.length; i++) {
                    if(orders[i] == 'U') {
                        if(cursor == 1) {
                            cursor = n;
                        }else {
                            cursor--;
                        }
                    }else {
                        if(cursor == n) {
                            cursor = 1;
                        }else {
                            cursor++;
                        }
                    }
                }
                for (int i = 1; i <= n; i++) {
                    System.out.print(i + " ");
                }
                System.out.println();
                System.out.println(cursor);
            }else {
                int head = 1;
                for (int i = 0; i < orders.length; i++) {
                    if(orders[i] == 'U') {
                        if(cursor == 1) {
                            cursor = n;
                            head = n - 3;
                        }else {
                            cursor--;
                            if(cursor == head - 1) {
                                head--;
                            }
                        }
                    }else {
                        if(cursor == n) {
                            cursor = 1;
                            head = 1;
                        }else {
                            cursor++;
                            if(cursor == head + 4) {
                                head++;
                            }
                        }
                    }
                }
                for (int i = 0; i < 4; i++) {
                    System.out.print((head + i) + " ");
                }
                System.out.println();
                System.out.println(cursor);
            }
        }
    }









    public static Stack<Integer> disorder(int n, int k, Scanner scanner) {

        int[] arr1 = new int[n];
        int[] arr2 = new int[n];
        for (int i = 0; i < 2 * n; i++) {
            if(i < n) {
                arr1[i] = scanner.nextInt();
            }else {
                arr2[i - n] = scanner.nextInt();
            }
        }
        Stack<Integer> stack = new Stack<>();
        for (int i = n - 1; i >= 0; i--) {
            stack.push(arr2[i]);
            stack.push(arr1[i]);
        }
        k--;
        for (int i = 0; i < k; i++) {
            for (int j = 0; j < 2 * n; j++) {
                if(j < n) {
                    arr1[j] = stack.pop();
                }else {
                    arr2[j - n] = stack.pop();
                }
            }
            for (int p = n - 1; p >= 0; p--) {
                stack.push(arr2[p]);
                stack.push(arr1[p]);
            }
        }
        return stack;
    }


    public static void main1(String[] args) {
        Scanner scanner = new Scanner(System.in);
        while(scanner.hasNextInt()) {
            int T = scanner.nextInt();
            List<Stack<Integer>> list = new ArrayList<>(T);
            for (int i = 0; i < T; i++) {
                int n = scanner.nextInt();
                int k = scanner.nextInt();
                Stack<Integer> ret = disorder(n, k, scanner);
                list.add(ret);
            }
            for (int i = 0; i < T; i++) {
                Stack<Integer> stack = list.get(i);
                while(!stack.isEmpty()) {
                    System.out.print(stack.pop() + " ");
                }
                System.out.println();
            }
        }
    }

}
