package T4;

import java.util.Scanner;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: 马拉圈
 * Date: 2023-05-10
 * Time: 11:09
 */
public class Day38 {

    public static int getCount(int[][] arr, int i, int j) {
        if(i > arr.length - 1 || j > arr[0].length - 1 || arr[i][j] != 0) {
            return 0;
        }else {
            return i == arr.length - 1 && j == arr[0].length - 1 ?
                    1 : getCount(arr, i + 1, j) + getCount(arr, i, j + 1);
        }
    }

    public static int C(int n1, int n2) {
        int flag1 = n1;
        int flag2 = n2;
        while(n2 != 1) {
            flag1 *= n1 - 1;
            flag2 *= n2 - 1;
            n1--;
            n2--;
        }
        return flag1 / flag2;
    }
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        while(scanner.hasNextInt()) {
            int m = scanner.nextInt();
            int n = scanner.nextInt();
            int[][] arr = new int[m][n];
            int k = scanner.nextInt();
            for (int i = 0; i < k; i++) {
                int index1 = scanner.nextInt();
                int index2 = scanner.nextInt();
                arr[index1 - 1][index2 - 1]++;
            }
            int count = getCount(arr, 0, 0);
            int sum = C(m + n - 2, m - 1);
            System.out.printf("%.2f", 1.0 * count / sum);
        }
    }

    public static void main2(String args[]){
        Scanner sc = new Scanner(System.in);
        while(sc.hasNext()){
            int n = sc.nextInt();
            int m = sc.nextInt();
            int k = sc.nextInt();
            // 放置蘑菇,map多给一行一列，因为题目的位置是从(1,1)开始
            int[][] map = new int[n+1][m+1];
            while(0 != k){
                int x = sc.nextInt();
                int y = sc.nextInt();
                map[x][y] = 1;
                k--;
            }
            // dp来计算到dp[i][j]位置的概率
            double[][] dp = new double[n+1][m+1];
            dp[1][1] = 1.0;
            // 开始走
            // dp[i][j]: dp[i-1][j] 和 dp[i][j+1]的概率之后
            for(int i = 1; i <= n; ++i){
                for(int j = 1; j <= m; ++j){
                    // 除过起始位置
                    if(!(i == 1 && j == 1)){
                        dp[i][j] = dp[i-1][j]*(j==m? 1.0:0.5) + dp[i][j-1]*
                                (i==n? 1.0:0.5);
                    }
                    // 如果(i,j)位置有蘑菇，则无法到达
                    if(map[i][j] == 1){
                        dp[i][j] = 0.0;
                    }
                }
            }
            // 到达(n,m)的概率为：
            System.out.printf("%.2f\n", dp[n][m]);
        }
    }
    public static int getNumber(char[][] arr, int indexI, int indexJ) {
        int count = 1;
        arr[indexI][indexJ] = '@';
        if(indexI > 0 && arr[indexI - 1][indexJ] == '.') {
            count += getNumber(arr, indexI - 1, indexJ);
        }
        if(indexJ > 0 && arr[indexI][indexJ - 1] == '.') {
            count += getNumber(arr, indexI, indexJ - 1);
        }
        if(indexI < arr.length - 1 && arr[indexI + 1][indexJ] == '.') {
            count += getNumber(arr, indexI + 1, indexJ);
        }
        if(indexJ < arr[0].length - 1 && arr[indexI][indexJ + 1] == '.') {
            count += getNumber(arr, indexI, indexJ + 1);
        }
        return count;
    }

    public static void main1(String[] args) {
        Scanner scanner = new Scanner(System.in);
        while (scanner.hasNextInt()) {
            int m = scanner.nextInt();
            int n = scanner.nextInt();
            int indexI = 0;
            int indexJ = 0;
            char[][] arr = new char[m][n];
            for (int i = 0; i < m; i++) {
                String str = scanner.next();
                for (int j = 0; j < n; j++) {
                    arr[i][j] = str.charAt(j);
                    if(arr[i][j] == '@') {
                        indexI = i;
                        indexJ = j;
                    }
                }
            }
            int number = getNumber(arr, indexI, indexJ);
            System.out.println(number);
        }
    }


}
