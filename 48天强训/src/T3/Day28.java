package T3;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: 马拉圈
 * Date: 2023-04-20
 * Time: 17:44
 */
class Node {
    int value;
    Node next;
    public Node(int value) {
        this.value = value;
    }
}

public class Day28 {

    public static void show(int n) {
        if(n == 0) {
            return;
        }
        double init = Math.pow(5, n) - 4;
        System.out.print((long)init + " ");
        for (int i = 0; i < n; i++) {
            init = (init - 1) * 4 / 5;
        }
        System.out.println((long)(init + n));
        System.out.println((long)Math.pow(4, n) + n - 4);
    }

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int input = 0;
        List<Integer> list = new ArrayList<>();
        do {
            input = scanner.nextInt();
            list.add(input);
        }while(input != 0);
        for (int i = 0; i < list.size(); i++) {
            show(list.get(i));
        }
    }







    public static void main1(String[] args) {
        Scanner scanner = new Scanner(System.in);
        while(scanner.hasNextInt()) {
            int n = scanner.nextInt();
            Node head = null;
            Node cur = head;
            for (int i = 0; i < n; i++) {
                int value = scanner.nextInt();
                if(head == null) {
                    head = new Node(value);
                    cur = head;
                }else {
                    cur.next = new Node(value);
                    cur = cur.next;
                }
            }
            int left = scanner.nextInt();
            int right = scanner.nextInt();
            if(left < right) {
                Node cur1 = head;
                Node cur2 = head;
                cur = head;
                for (int i = 0; i < right - 1; i++) {
                    if(i < left - 1) {
                        cur1 = cur1.next;
                    }
                    if(i < left - 2) {
                        cur = cur.next;
                    }
                    cur2 = cur2.next;
                }
                Node current1 = cur1;
                Node current2 = cur2.next;
                if(cur == cur1) {
                    cur1 = cur1.next;
                    while(cur1 != current2) {
                        Node curNext = cur1.next;
                        cur1.next = head;
                        head = cur1;
                        cur1 = curNext;
                    }
                }else {
                    cur1 = cur1.next;
                    while(cur1 != current2) {
                        Node curNext = cur1.next;
                        cur1.next = cur.next;
                        cur.next = cur1;
                        cur1 = curNext;
                    }
                }
                current1.next = cur1;
            }
            for (int i = 0; i < n; i++) {
                System.out.print(head.value + " ");
                head = head.next;
            }
            System.out.println();
        }

    }
}
