#include "basis.h"

void change(int i) {
	switch (i) {
		case 1:
			printf("年卡");
			break;
		case 2:
			printf("月卡");
			break;
		case 3 :
			printf("日卡");
			break;
		case 4 :
			printf("时卡");
			break;
		default:
			break;
	}
}

//修改方案:
//1. 一小时标准不存在时，规定一个默认值，6/h
//2. 结账的时候，如果一小时的标准存在，则使用此标准！


void addStandard(Standard* pcs) {
	int input = 0;
	menu2_2();
	printf("请输入待增加的计费标准的类型:>");
	scanf("%d", &input);
	if (input <= 4 && input >= 1 && pcs[input - 1].state != 1) {
		printf("请输入你的计费标准单价是多少元:>");
		scanf("%lf", &pcs[input - 1].price);
		pcs[input - 1].state = 1;
		printf("新增成功\n");
	}
	else {
		printf("此标准无法加入，“可能”是已有此标准，但可进行修改操作\n");
	}
}
void search(Standard* pcs) {
	int input = 0;
	menu2_2();
	printf("请输入待查看的计费标准的类型:>");
	scanf("%d", &input);
	if (input <= 4 && input >= 1 && pcs[input - 1].state == 1) {
		printf("计费标准为："); 
		change(input);
		printf("=》单位时间内收费%.2lf元\n", pcs[input - 1].price);
	}
	else {
		printf("暂无此标准\n");
	}
}
void delStandard(Standard* pcs) {
	int input = 0;
	menu2_2();
	printf("请输入待删除计费标准的类型:>");
	scanf("%d", &input);
	if (input <= 4 && input >= 1 && pcs[input - 1].state == 1) {
		pcs[input - 1].state = 0;
		printf("删除成功\n");
	}
	else {
		printf("删除失败\n");
	}
}
void modify(Standard* pcs) {
	int input = 0;
	menu2_2();
	printf("请输入待修改计费标准的类型:>");
	scanf("%d", &input);
	if (input <= 4 && input >= 1 && pcs[input - 1].state == 1) {
		printf("请输入你的调整后的计费标准单价是多少元:>");
		scanf("%lf", &pcs[input - 1].price);
		printf("修改成功\n");
	}
	else {
		printf("暂无此标准\n");
	}	
}

void chargeStandard(Manager* pm) {
	int input = 0;
	Standard* pcs = standardCarry();
	void (*func[5])(Standard*) = { exitOutStandard, addStandard, search, delStandard, modify};
	do {
		menu2_1();
		scanf("%d", &input);
		if (input <= 4 && input >= 0) {
			func[input](pcs);
		}
		else {
			printf("请重新输入\n");
		}
	} while (input);
}