#include "basis.h"


int orderNumber = 0;
int workNumber = 0;

void clockIn(Card* cur, Puncher* ppu, size_t t) {
	Puncher* newOne = (Puncher*)malloc(sizeof(Puncher));
	newOne->id = cur->id;
	newOne->next = NULL;
	newOne->timeNode = t;//t为正确的上机时间

	if (workNumber == 0) {
		memcpy(ppu, newOne, sizeof(Puncher));
	}
	else {
		Puncher* current = ppu;
		while (current->next != NULL) {
			current = current->next;
		}
		current->next = newOne;
	}
	workNumber++;
}

void onComputer(Card* pc, Puncher* ppu, Consume* psu, Standard* pcs) {
	printf("请输入卡号与密码:>");
	Card* cur = pc;
	int id = 0;
	char password[7];
	scanf("%d%s", &id, password);
	while (cardNumber != 0 && cur != NULL) {
		if (cur->id == id && strcmp(password, cur->password) == 0) {
			if (cur->effect != 1) {
				printf("此卡已被注销,无法上机\n");
				return;
			}
			else {
				if (cur->state == 1) {
					printf("已经是上机状态，本次操作失效\n");
					return;
				}
				cur->state = 1;
				time_t t = time(NULL);
				//上机记录~~
				if (pcs[cur->cardType - 1].state == 0 || cur->balance < pcs[cur->cardType - 1].price) {
					printf("无法上机\n");
					return;
				}
				if (t - cur->upTime >= transfer(cur->cardType)) {
					settlement(cur, psu, t, 1, pcs);
					cur->upTime = t;
				}
				clockIn(cur, ppu, t);
				//上机记录~~
				printf("--------------------------\n");
				printf("上机成功\n");
				printf("时间:%s", ctime(&t));
				printf("--------------------------\n");
			}
			return;
		}
		cur = cur->next;
	}
	printf("上机失败,“可能”原因是此卡暂为开通\n");
}

//3600------1h
//86400-----1day
//2592000---1month//默认30天
//31536000--1year//默认365天
long long transfer(int i) {
	switch (i) {
		case 1 :	
			return 31536000;
		case 2 :	
			return 2592000;
		case 3 :	
			return 86400;
		case 4 :	
			return 3600;
	}
}
//不足一年按一年
// 则benifit = 固定费用 + 超时费用（gap * 1小时费用）
// 在这里去写，超时按时间扣费

void settlement(Card* cur, Consume* psu, time_t t, double gap, Standard* pcs) {
	double benifit = gap * pcs[cur->cardType - 1].price;
	cur->balance -= benifit;
	Consume* newOne = (Consume*)malloc(sizeof(Consume));
	newOne->id = cur->id;
	newOne->money = benifit;
	newOne->next = NULL;
	newOne->timeNode = t;

	if (orderNumber == 0) {
		memcpy(psu, newOne, sizeof(Consume));
	}
	else {
		Consume* current = psu;
		while (current->next != NULL) {
			current = current->next;
		}
		current->next = newOne;
	}
	orderNumber++;
}
void settlementHours(Card* cur, Consume* psu, time_t t, double gap, Standard* pcs) {
	int price = 6;
	if (pcs[3].state == 1) {
		price = pcs[3].price;
	}
	double benifit = gap * price;
	cur->balance -= benifit;
	Consume* newOne = (Consume*)malloc(sizeof(Consume));
	newOne->id = cur->id;
	newOne->money = benifit;
	newOne->next = NULL;
	newOne->timeNode = t;

	if (orderNumber == 0) {
		memcpy(psu, newOne, sizeof(Consume));
	}
	else {
		Consume* current = psu;
		while (current->next != NULL) {
			current = current->next;
		}
		current->next = newOne;
	}
	orderNumber++;
}

void offComputer(Card* pc, Consume* psu, Standard* pcs) {
	printf("请输入卡号与密码:>");
	Card* cur = pc;
	int id = 0;
	char password[7];
	scanf("%d%s", &id, password);
	while (cardNumber != 0 && cur != NULL) {
		if (cur->id == id && strcmp(password, cur->password) == 0) {
			if (cur->state == 0) {
				printf("已经是下机状态，本次操作失效\n");
				return;
			}
			time_t t = time(NULL);
			long long longTime = t - cur->upTime;
			double gap = 1.0 * longTime / transfer(cur->cardType);
			double hours = 0;
			if (gap > 1) {
				hours = 1.0 * (longTime - transfer(cur->cardType)) / transfer(4);//不足进一
				if (hours != (int)hours) {
					hours = (int)hours + 1;
				}
			}
			if (hours != 0) {
				settlementHours(cur, psu, t, hours, pcs);
			}
			cur->state = 0;
			if (cur->balance < 0) {
				printf("此卡已欠费，账号已被注销，请充值缴费\n");
				cur->effect = 0;
			}
			printf("--------------------------\n");
			printf("下机成功, 账户已更新\n");//下机则结算
			printf("时间:%s", ctime(&t));
			printf("--------------------------\n");
			return;
		}
		cur = cur->next;
	}
	printf("下机失败,“可能”原因是此卡暂为开通\n");
}

void chargeManage(Manager* pm) {
	int input = 0;
	Card* pc = cardCarry();
	Consume* psu = consumeCarry();
	Puncher* ppu = puncherCarry();
	Standard* pcs = standardCarry();
	do {
		menu3_1();
		scanf("%d", &input);
		switch (input) {
		case 1 :
			onComputer(pc, ppu, psu, pcs);
			break;
		case 2 :
			offComputer(pc, psu, pcs);
			break;
		default:
			printf("输入错误\n");
			break;
		case 0:
			exitOutCard(pc);
			exitOutConsume(psu);
			exitOutPuncher(ppu);
			exitOutStandard(pcs);
			printf("退出成功\n");
			break;
		}
	} while (input);
}