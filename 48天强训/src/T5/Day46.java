package T5;

import java.util.*;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: 马拉圈
 * Date: 2023-05-17
 * Time: 13:21
 */
public class Day46{

    public int MoreThanHalfNum_Solution (int[] numbers) {
        Arrays.sort(numbers);
        return numbers[numbers.length / 2];
    }


    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        List<String> list = new ArrayList<>();
        List<Integer> integerList = new ArrayList<>();
        while(scanner.hasNext()) {
            list.add(scanner.next());
            integerList.add(scanner.nextInt());
        }
        for (int i = 0; i < list.size(); i++) {
            String[] strings = list.get(i).split("\\\\");
            String string = strings[strings.length - 1];
            if(string.length() > 16) {
                string = string.substring(string.length() - 16);
            }
            list.set(i, string);
        }
        List<Node> nodes = new ArrayList<>();
        for (int i = 0; i < list.size(); i++) {
            Node newOne = new Node(list.get(i), integerList.get(i), 1);
            int j = 0;
            for (; j < i && j < nodes.size(); j++) {
                if(nodes.get(j).equals(newOne)) {
                    nodes.get(j).count++;
                    break;
                }
            }
            if(!(j < i && j < nodes.size())) {
                nodes.add(newOne);
            }
        }
        if(nodes.size() > 8) {
            nodes = nodes.subList(nodes.size() - 8, nodes.size());
            for (int i = 0; i < 8; i++) {
                Node node = nodes.get(i);
                System.out.println(node.string + " "
                        + node.number + " " + node.count);
            }
        }else {
            for (int i = 0; i < nodes.size(); i++) {
                Node node = nodes.get(i);
                System.out.println(node.string + " "
                        + node.number + " " + node.count);
            }
        }





    }
}
class Node {
    String string;
    int number;
    int count;

    public Node(String string, int number, int count) {
        this.string = string;
        this.number = number;
        this.count = count;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Node node = (Node) o;
        return number == node.number && Objects.equals(string, node.string);
    }


}
