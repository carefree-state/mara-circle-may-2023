package T1;

import java.lang.reflect.Array;
import java.util.Scanner;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: 马拉圈
 * Date: 2023-03-27
 * Time: 20:02
 */
public class Day8 {

    public static void main1(String[] args) {
        Scanner scanner = new Scanner(System.in);
        while(scanner.hasNextInt()) {
            int number = scanner.nextInt();
            scanner.nextLine();
            if(number == 0) {
                System.out.println("none");
                continue;
            }
            boolean flag1 = true;
            boolean flag2 = true;
            String prev = scanner.nextLine();
            for (int i = 1; i < number; i++) {
                String str = scanner.nextLine();
                if(str.length() < prev.length()) {
                    flag2 = false;
                }
                if(prev.compareTo(str) > 0) {
                    flag1 = false;
                }
                prev = str;
            }
            if(flag1 && flag2) {
                System.out.println("both");
            }else if(flag1 && !flag2) {
                System.out.println("lexicographically");
            }else if(!flag1 && flag2) {
                System.out.println("lengths");
            }else {
                System.out.println("none");
            }

        }
    }



    public static void main2(String[] args) {
        Scanner scanner = new Scanner(System.in);
        while(scanner.hasNextInt()) {
            int A = scanner.nextInt();
            int B = scanner.nextInt();
            int ab = A * B;
            while(A % B != 0) {
                int C = A % B;
                A = B;
                B = C;
            }
            System.out.println(ab / B);
        }
    }



    public static void main(String[] args) {
        int ret = new B().getValue();
        System.out.println(ret);//7
    }
    static class A {
        protected int value;
        public A(int v) {
            setValue(v);
        }
        public void setValue(int value) {
            this.value = value;
        }
        public int getValue() {
            try{
                value++;
                return value;
            }catch(Exception e) {
                System.out.println(e.toString());
            }finally {
                this.setValue(value);
                System.out.println(value);//6 7
            }
            return value;
        }
    }
    static class B extends A {
        public B() {
            super(5);
            setValue(getValue() - 3);
        }
        public void setValue(int value) {
            super.setValue(2 * value);
        }
    }


}
