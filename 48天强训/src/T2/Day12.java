package T2;

import java.util.Scanner;
import java.util.concurrent.locks.ReentrantLock;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: 马拉圈
 * Date: 2023-04-01
 * Time: 18:33
 */
public class Day12 {


    public static boolean isPrime(int number) {
        if(number < 2) {
            return false;
        }
        double bound = Math.sqrt(number);
        for (int i = 2; i <= bound; i++) {
            if(number % i == 0) {
                return false;
            }
        }
        return true;
    }

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        while(scanner.hasNextInt()) {
            int n = scanner.nextInt();
            int number =  n / 2;
            while(number != 1) {
                if(isPrime(number) && isPrime(n - number)) {
                    System.out.println(number);
                    System.out.println(n - number);
                    break;
                }
                number--;
            }
        }
    }


    public static int binInsert(int n, int m, int j, int i) {
        return n | (m << j);
    }
    public static void main1(String[] args) {
        System.out.println(binInsert(1024, 19, 2, 6));
    }

}

