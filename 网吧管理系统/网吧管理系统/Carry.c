#include "basis.h"

Card* cardCarry() {
	FILE* pf = fopen("data\\card.txt", "rb");
	Card* pc = (Card*)malloc(sizeof(Card));//堆区空间，不会被收回
	if (pf == NULL) {
		pf = fopen("data\\card.txt", "wb");
		fwrite(&cardNumber, sizeof(int), 1, pf);
	}
	else {
		fread(&cardNumber, sizeof(int), 1, pf);
		fread(pc, sizeof(Card), 1, pf);
		Card* cur = pc;
		for (int i = 1; i < cardNumber; i++) {
			Card* newOne = (Card*)malloc(sizeof(Card));
			fread(newOne, sizeof(Card), 1, pf);
			newOne->next = NULL;
			cur->next = newOne;
			cur = cur->next;
		}
	}
	fclose(pf);
	return pc;
}

Standard* standardCarry() {
	FILE* pf = fopen("data\\rate.txt", "rb");
	Standard* pcs = (Standard*)malloc(4 * sizeof(Standard));
	if (pf == NULL) {
		pf = fopen("data\\rate.txt", "wb");
		fwrite(pcs, sizeof(Standard), 4, pf);
	}
	else {
		fread(pcs, sizeof(Standard), 4, pf);
	}
	fclose(pf);
	return pcs;
}

Consume* consumeCarry() {
	FILE* pf = fopen("data\\consume.txt", "rb");
	Consume* psu = (Consume*)malloc(sizeof(Consume));//堆区空间，不会被收回
	if (pf == NULL) {
		pf = fopen("data\\consume.txt", "wb");
		fwrite(&orderNumber, sizeof(int), 1, pf);
	}
	else {
		fread(&orderNumber, sizeof(int), 1, pf);
		fread(psu, sizeof(Consume), 1, pf);
		Consume* cur = psu;
		for (int i = 1; i < orderNumber; i++) {
			Consume* newOne = (Consume*)malloc(sizeof(Consume));
			fread(newOne, sizeof(Consume), 1, pf);
			newOne->next = NULL;
			cur->next = newOne;
			cur = cur->next;
		}
	}
	fclose(pf);
	return psu;
}


Puncher* puncherCarry() {
	FILE* pf = fopen("data\\puncher.txt", "rb");
	Puncher* ppu = (Puncher*)malloc(sizeof(Puncher));//堆区空间，不会被收回
	if (pf == NULL) {
		pf = fopen("data\\puncher.txt", "wb");
		fwrite(&workNumber, sizeof(int), 1, pf);
	}
	else {
		fread(&workNumber, sizeof(int), 1, pf);
		fread(ppu, sizeof(Puncher), 1, pf);
		Puncher* cur = ppu;
		for (int i = 1; i < workNumber; i++) {
			Puncher* newOne = (Puncher*)malloc(sizeof(Puncher));
			fread(newOne, sizeof(Puncher), 1, pf);
			newOne->next = NULL;
			cur->next = newOne;
			cur = cur->next;
		}
	}
	fclose(pf);//C语言文件重复打开，会自动关闭之前的文件，但是在这样写不规范
	//并且写文件不关闭，会导致文件不被更新~
	//虽然读文件操作不受限制，但是也占用内存资源了
	//规范：要关闭！
	return ppu;
}



