package T1;

import java.util.Scanner;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: 马拉圈
 * Date: 2023-03-29
 * Time: 11:17
 */
public class Day9 {


    public static int C(int n, int m) {
        int up = 1;
        int down = 1;
        while(m != 0) {
            up *= n;
            down *= m;
            n--;
            m--;
        }
        return up / down;
    }

    public static double CC(double n, double m){
        return m != 0 ? (n / m) * CC(n-1, m-1) : 1;//n/m可能为小数，而整形比整形还是整形~
    }

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        while(scanner.hasNextInt()) {
            int m = scanner.nextInt();
            int n = scanner.nextInt();
            double ret1 = CC(n + m, m);
            int ret2 = C(n + m, m);
            System.out.println((int)ret1);
            System.out.println(ret2);
            int[] arr = new int[10];
            for(int x : arr) {
                System.out.print(x);
                break;
            }

        }
    }

    public static void main2(String[] args) {
        String str = "abc";
        char ch = '是';
        str.toUpperCase();
        System.out.println(str);
    }

    public static int add(int a, int b) {
        if(a < 0) {
            while(0 != a++) {
                b--;
            }
        }else if(a > 0) {
            while(0 != a--) {
                b++;
            }
        }
        return b;
    }
}
