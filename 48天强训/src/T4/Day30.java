package T4;

import java.util.HashSet;
import java.util.Scanner;
import java.util.Set;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: 马拉圈
 * Date: 2023-04-22
 * Time: 10:31
 */
public class Day30 {


    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        while(scanner.hasNextLine()) {
            String word = scanner.nextLine();
            StringBuilder stringBuilder = new StringBuilder();
            for (int i = 0; i < word.length(); i++) {
                char ch = word.charAt(i);
                if(ch == ' ') {
                    stringBuilder.append(ch);
                }else {
                    int real = (ch + 21 - 'A') % 26 + 'A';
                    stringBuilder.append((char)real);
                }
            }
            System.out.println(stringBuilder.toString());
        }
    }











    public static boolean judge(int n) {
        if(n <= 1) {
            return false;
        }
        for (int i = 2; i <= Math.sqrt(n); i++) {
            if(n % i == 0) {
                return false;
            }
        }
        return true;
    }

    public static void main1(String[] args) {
        Scanner scanner = new Scanner(System.in);
        while(scanner.hasNextInt()) {
            int n = scanner.nextInt();
            int count = 0;
            if(judge(n)) {
                System.out.println(1);
                continue;
            }
            for (int i = 2; i <= Math.sqrt(n); i++) {//不是素数，就肯定有小于等于根号的因子
                if(n % i == 0 && judge(i)) {
                    count++;
                    while(n % i == 0) {
                        n /= i;
                    }
                    if(n == 1) {
                        System.out.println(count);
                        break;
                    }
                    if(judge(n)) {
                        System.out.println(count + 1);
                        break;
                    }
                }
            }
        }
    }

}
