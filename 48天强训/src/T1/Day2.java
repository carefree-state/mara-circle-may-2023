package T1;

import java.util.List;
import java.util.Scanner;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: 马拉圈
 * Date: 2023-03-20
 * Time: 23:30
 */
public class Day2 {


    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int prev = 0;
        while(scanner.hasNextInt()) {
            int number = scanner.nextInt();
            int[] arr = new int[number];
            for (int i = 0; i < number; i++) {
                int tmp = scanner.nextInt();
                arr[i] = tmp - prev;
                prev = tmp;
            }
            arr[0] = 0;
            int times = 1;
            for (int i = 1; i < number; i++) {
                if(arr[i] * arr[i - 1] < 0) {
                    arr[i] = 0;
                    times++;
                }
            }
            System.out.println(times);
        }
    }






    public static void main1(String[] args) {
        Scanner scanner = new Scanner(System.in);
        while(scanner.hasNextLine()) {
            String str = scanner.nextLine();
            String[] strings = str.split(" ");
            StringBuilder stringBuilder = new StringBuilder();
            for (int i = strings.length - 1; i >= 0; i--) {
                stringBuilder.append(strings[i]);
                stringBuilder.append(" ");
            }
            String ret = stringBuilder.toString();
            System.out.println(ret);
        }
    }
}
